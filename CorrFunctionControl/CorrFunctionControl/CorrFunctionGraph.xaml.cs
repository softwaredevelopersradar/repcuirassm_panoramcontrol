﻿using Arction.Wpf.SemibindableCharting;
using Arction.Wpf.SemibindableCharting.SeriesXY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Arction.Wpf.SemibindableCharting.Views;

namespace CorrFunctionControl
{
    /// <summary>
    /// Логика взаимодействия для CorrFunctionGraph.xaml
    /// </summary>
    public partial class CorrFunctionGraph : UserControl
    {
        public delegate void ThresholdChanged(double value);
        public event ThresholdChanged OnThresholdChanged = (value) => { };

        private double _xLengthMin = -8000;
        public double XAxesCorrGraphSetMin 
        {
            get => _xLengthMin;
            set 
            {
                _xLengthMin = value;
                UpdateGraphProperties();
            }
        }

        private double _xLengthMax = 8000;
        public double XAxesCorrGraphSetMax 
        {
            get => _xLengthMax;
            set 
            {
                _xLengthMax = value;
                UpdateGraphProperties();
            }
        }

        readonly double YLengthMax = 1;
        readonly double YLengthMin = 0;

        private float _countPoint = 2001;
        public float CountPointsSet 
        {
            get => _countPoint;
            set 
            {
                _countPoint = value;
                UpdateGraphProperties();
            }
        }

        private double FirstSample { get; set; } = -8000;
        private double SamplingFrequency { get; set; } = 2000 / 16000d;

        public CorrFunctionGraph()
        {
            InitializeComponent();
            InitGraphsProperies();
            //lFreq1.HorizontalContentAlignment = HorizontalAlignment.Left;
            //lBand1.HorizontalContentAlignment = HorizontalAlignment.Left;
            lFreq2.HorizontalContentAlignment = HorizontalAlignment.Left;
            lBand2.HorizontalContentAlignment = HorizontalAlignment.Left;
            lB1.Text = "\u0394" + "F =";
            lB2.Content = "\u0394" + "F =";
            lTime1.Text = "\u03C4" + " =";
            lTime2.Content = "\u03C4" + " =";
            GridLengthConverter = new GridLengthConverter();
            delOne.Text = "\u03C4" + "1 = ";
            delTwo.Text = "\u03C4" + "2 = ";
            delThree.Text = "\u03C4" + "3 = ";
            delFour.Text = "\u0394" + "\u03C4" + " =";
        }
        
        private void InitGraphsProperies()
        {
            var graphOne = GrapfOne.ViewXY;
            LegendBoxOne.Visible = false;
            graphOne.SampleDataSeries[0].Title.Text = "1-1";
            graphOne.SampleDataSeries[1].Title.Text = "1-2";
            graphOne.SampleDataSeries[2].Title.Text = "1-3";
            graphOne.SampleDataSeries[3].Title.Text = "1-4";   
            TdLine.ShowInLegendBox = false;
            
            CursorYOne.ShowInLegendBox = false;            

            var graphTwo = GraphTwo.ViewXY;
            LegendBox.Visible = false;
            graphTwo.SampleDataSeries[0].Title.Text = "1-1";
            graphTwo.SampleDataSeries[1].Title.Text = "1-2";
            graphTwo.SampleDataSeries[2].Title.Text = "1-3";
            graphTwo.SampleDataSeries[3].Title.Text = "1-4";
            CursorYOneM.ShowInLegendBox = false;
            TdLineAuto.ShowInLegendBox = false;

            
        }

        private void UpdateGraphProperties() 
        {
            var graphOne = GrapfOne.ViewXY;
            graphOne.XAxes[0].Minimum = XAxesCorrGraphSetMin;
            graphOne.XAxes[0].Maximum = XAxesCorrGraphSetMax;
            graphOne.YAxes[0].Minimum = YLengthMin;
            graphOne.YAxes[0].Maximum = YLengthMax;

            var graphTwo = GraphTwo.ViewXY;
            graphTwo.XAxes[0].Minimum = XAxesCorrGraphSetMin;
            graphTwo.XAxes[0].Maximum = XAxesCorrGraphSetMax;
            graphTwo.YAxes[0].Minimum = YLengthMin;
            graphTwo.YAxes[0].Maximum = YLengthMax;

            FirstSample = XAxesCorrGraphSetMin;
            SamplingFrequency = (CountPointsSet - 1) / (XAxesCorrGraphSetMax * 2);
        }

        public void Converter( double[] Points, byte ID, byte Graph, int Frequency, int Bandwidth )
        {
            switch (Graph)
            {
                case 0:
                    DrawCorrFunction(Points, ID, Frequency, Bandwidth);
                    break;
                case 1:
                    DrawCorrFuncSinhron(Points, ID, Frequency, Bandwidth);
                    break;
            }
        }

        private void DrawCorrFunction( double[] Points, byte ID, int Frequency, int Bandwidth)
        {
            DispatchIfNecessary(() => 
            {
                lFreq1.Text = (Frequency / 1000.0).ToString() + " MHz";
                lBand1.Text = (Bandwidth / 1000.0).ToString() + " MHz";
                DrawGraphManual(Points, ID);
            });            
        }

        private void DrawGraphManual(double[] Points, byte ID) 
        {
            GrapfOne.BeginUpdate();
            var _view = GrapfOne.ViewXY;
            switch (ID)
            {
                case 0:
                    _view.SampleDataSeries[0].FirstSampleTimeStamp = FirstSample;
                    _view.SampleDataSeries[0].SamplingFrequency = SamplingFrequency;
                    _view.SampleDataSeries[0].InvalidateData();
                    _view.SampleDataSeries[0].SamplesDouble = Points;
                    break;
                case 1:
                    _view.SampleDataSeries[1].FirstSampleTimeStamp = FirstSample;
                    _view.SampleDataSeries[1].SamplingFrequency = SamplingFrequency;
                    _view.SampleDataSeries[1].InvalidateData();
                    _view.SampleDataSeries[1].SamplesDouble = Points;
                    break;
                case 2:
                    _view.SampleDataSeries[2].FirstSampleTimeStamp = FirstSample;
                    _view.SampleDataSeries[2].SamplingFrequency = SamplingFrequency;
                    _view.SampleDataSeries[2].InvalidateData();
                    _view.SampleDataSeries[2].SamplesDouble = Points;
                    break;
                case 3:
                    _view.SampleDataSeries[3].FirstSampleTimeStamp = FirstSample;
                    _view.SampleDataSeries[3].SamplingFrequency = SamplingFrequency;
                    _view.SampleDataSeries[3].InvalidateData();
                    _view.SampleDataSeries[3].SamplesDouble = Points;
                    break;
            }

            GrapfOne.EndUpdate();
        }

        private void DrawCorrFuncSinhron(double[] Points, byte ID, int Frequency, int Bandwidth)
        {
            DispatchIfNecessary(() =>
            {
                GraphTwo.BeginUpdate();
                var _view = GraphTwo.ViewXY;
                lFreq2.Content = (Frequency / 1000.0).ToString() + " MHz";
                lBand2.Content = (Bandwidth / 1000.0).ToString() + " MHz";

                switch (ID)
                {
                    case 0:
                        _view.SampleDataSeries[0].FirstSampleTimeStamp = FirstSample;
                        _view.SampleDataSeries[0].SamplingFrequency = SamplingFrequency;
                        _view.SampleDataSeries[0].InvalidateData();
                        _view.SampleDataSeries[0].SamplesDouble = Points;
                        break;
                    case 1:
                        _view.SampleDataSeries[1].FirstSampleTimeStamp = FirstSample;
                        _view.SampleDataSeries[1].SamplingFrequency = SamplingFrequency;
                        _view.SampleDataSeries[1].InvalidateData();
                        _view.SampleDataSeries[1].SamplesDouble = Points;
                        break;
                    case 2:
                        _view.SampleDataSeries[2].FirstSampleTimeStamp = FirstSample;
                        _view.SampleDataSeries[2].SamplingFrequency = SamplingFrequency;
                        _view.SampleDataSeries[2].InvalidateData();
                        _view.SampleDataSeries[2].SamplesDouble = Points;
                        break;
                    case 3:
                        _view.SampleDataSeries[3].FirstSampleTimeStamp = FirstSample;
                        _view.SampleDataSeries[3].SamplingFrequency = SamplingFrequency;
                        _view.SampleDataSeries[3].InvalidateData();
                        _view.SampleDataSeries[3].SamplesDouble = Points;
                        break;
                }

                GraphTwo.EndUpdate();
            });
        }

        private void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }

        private void GrapfOne_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (LegendBoxOne.Visible == true)
            {
                LegendBoxOne.Visible = false;
            }
            else { LegendBoxOne.Visible = true; }
        }

        private void GraphTwo_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (LegendBox.Visible == true)
            {
                LegendBox.Visible = false;
            }
            else { LegendBox.Visible = true; }
        }

        private void GrapfOne_MouseMove(object sender, MouseEventArgs e)
        {
            //SetMarkerOne(e.GetPosition(GrapfOne));
        }

        private void GrapfTwo_MouseMove(object sender, MouseEventArgs e)
        {
            //SetMarkerTwo(e.GetPosition(GraphTwo));
        }

        private void SetMarkerOne(Point point) 
        {
            GrapfOne.BeginUpdate();

            var _view = GrapfOne.ViewXY;
            var m1 = _view.SampleDataSeries[0].SeriesEventMarkers[0];

            xAx1.CoordToValue((int)point.X, out var xValue, true);
            yAx1.CoordToValue((int)point.Y, out var yValue, true);
            
            m1.XValue = xValue;
            m1.YValue = yValue;
            m1.Label.Text = m1.XValue.ToString("0.0");
            if(xValue <= 50) { m1.Label.Distance = 7; }
            else { m1.Label.Distance = -30; }
            
            GrapfOne.EndUpdate();
        }

        private void SetMarkerTwo(Point point)
        {
            GraphTwo.BeginUpdate();

            var _view = GraphTwo.ViewXY;
            var m1 = _view.SampleDataSeries[0].SeriesEventMarkers[0];

            xAx1.CoordToValue((int)point.X, out var xValue, true);
            yAx1.CoordToValue((int)point.Y, out var yValue, true);

            m1.XValue = xValue;
            m1.YValue = yValue;
            m1.Label.Text = m1.XValue.ToString("0.0");
            if (xValue <= 50) { m1.Label.Distance = 7; }
            else { m1.Label.Distance = -30; }

            GraphTwo.EndUpdate();
        }

        private void TdLine_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            if (yAx1 == null) return;
            if (e.NewValue > yAx1.Maximum) 
            {
                TdLine.Value = yAx1.Maximum;
                return;
            }
            if (e.NewValue < yAx1.Minimum) 
            {
                TdLine.Value = yAx1.Minimum;
                return;
            }

            //TdLineAuto.Value = TdLine.Value;
            OnThresholdChanged?.Invoke(e.NewValue);
        }

        public void UpdateCorrelationTheresold(double theresold)
        {
            try
            {
                TdLine.Value = theresold;
            }
            catch { }
        }

        private void TdLineAuto_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            if (yAx2 == null) return;
            if (e.NewValue > yAx2.Maximum)
            {
                TdLineAuto.Value = yAx2.Maximum;
                return;
            }
            if (e.NewValue < yAx2.Minimum)
            {
                TdLineAuto.Value = yAx2.Minimum;
                return;
            }
            TdLine.Value = TdLineAuto.Value;
        }
    }
}
