﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using YamlDotNet.Serialization;

namespace CorrFunctionControl
{
    public partial class CorrFunctionGraph
    {
        GridLengthConverter GridLengthConverter;
        private string heightV = "20";
        private string heightUnV = "0";
        private string fileName = "";
        private int ExpIndex = 0;
        List<Expertise> expertises;

        byte debugVisible = 0;
        public byte DebugVisible 
        {
            get => debugVisible;
            set 
            {
                debugVisible = value;
                VisiblePanels(debugVisible);
            }
        }

        private void VisiblePanels(byte index) 
        {
            switch (index)
            {
                case 0:                    
                    gridUp.RowDefinitions[0].Height = (GridLength)GridLengthConverter.ConvertFromString(heightUnV);
                    CursorXTwo.Visible = false;
                    break;
                case 1:
                    gridUp.RowDefinitions[0].Height = (GridLength)GridLengthConverter.ConvertFromString(heightV);
                    CursorXTwo.Visible = true;
                    break;
                default:
                    break;
            }
        }

        public void UpdateCorrelationTaus(double tau1, double tau2, double tau3)
        {
            DispatchIfNecessary(() => 
            {
                delOneData.Text = string.Format("{0:0.0}", tau1);
                delTwoData.Text = string.Format("{0:0.0}", tau2);
                delThreeData.Text = string.Format("{0:0.0}", tau3);
            });
        }

        private void BOpenFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Filter = "Yaml File (*.yaml)| *.yaml"
            };
            if (openFileDialog.ShowDialog() != true) return;
            fileName = openFileDialog.FileName;
            ExpIndex = 0;
            LoadFile(fileName, ExpIndex);
        }

        private void BClickPrev_Click(object sender, RoutedEventArgs e)
        {            
            if (expertises != null)
            {
                ExpIndex--;
                if (ExpIndex < 0) ExpIndex = 0;

                lFreq1.Text = string.Format("{0:0.000}", (expertises[ExpIndex].FreqkHz / 1000f)) + " MHz";
                lBand1.Text = string.Format("{0:0.000}", (expertises[ExpIndex].BandwidthkHz / 1000.0)) + " MHz";
                delOneData.Text = string.Format("{0:0.0}", expertises[ExpIndex].Tau1 * 1e9);
                delTwoData.Text = string.Format("{0:0.0}", expertises[ExpIndex].Tau2 * 1e9);
                delThreeData.Text = string.Format("{0:0.0}", expertises[ExpIndex].Tau3 * 1e9);
                delIndData.Text = ExpIndex.ToString();
                if (expertises[ExpIndex].Corr1_1 != null) DrawGraphManual(expertises[ExpIndex].Corr1_1, 0);
                if (expertises[ExpIndex].Corr2_1 != null) DrawGraphManual(expertises[ExpIndex].Corr2_1, 1);
                if (expertises[ExpIndex].Corr3_1 != null) DrawGraphManual(expertises[ExpIndex].Corr3_1, 2);
                if (expertises[ExpIndex].Corr4_1 != null) DrawGraphManual(expertises[ExpIndex].Corr4_1, 3);
            }
        }

        private void BClickNext_Click(object sender, RoutedEventArgs e)
        {            
            if (expertises != null)
            {
                ExpIndex++;
                if (ExpIndex >= expertises.ToArray().Length) ExpIndex = expertises.ToArray().Length - 1;

                lFreq1.Text = string.Format("{0:0.000}", (expertises[ExpIndex].FreqkHz / 1000f)) + " MHz";
                lBand1.Text = string.Format("{0:0.000}", (expertises[ExpIndex].BandwidthkHz / 1000.0)) + " MHz";
                delOneData.Text = string.Format("{0:0.0}", expertises[ExpIndex].Tau1 * 1e9);
                delTwoData.Text = string.Format("{0:0.0}", expertises[ExpIndex].Tau2 * 1e9);
                delThreeData.Text = string.Format("{0:0.0}", expertises[ExpIndex].Tau3 * 1e9);
                delIndData.Text = ExpIndex.ToString();
                if (expertises[ExpIndex].Corr1_1 != null) DrawGraphManual(expertises[ExpIndex].Corr1_1, 0);
                if (expertises[ExpIndex].Corr2_1 != null) DrawGraphManual(expertises[ExpIndex].Corr2_1, 1);
                if (expertises[ExpIndex].Corr3_1 != null) DrawGraphManual(expertises[ExpIndex].Corr3_1, 2);
                if (expertises[ExpIndex].Corr4_1 != null) DrawGraphManual(expertises[ExpIndex].Corr4_1, 3);
            }
        }

        private void LoadFile(string filePath, int index) 
        {            
            var loadFile = YamlLoad<ReverseExpertise>(filePath);            
            expertises = new List<Expertise>(loadFile.Expertises);

            lFreq1.Text = string.Format("{0:0.000}", (expertises[ExpIndex].FreqkHz / 1000f)) + " MHz";
            lBand1.Text = string.Format("{0:0.000}", (expertises[ExpIndex].BandwidthkHz / 1000.0)) + " MHz";

            delOneData.Text = string.Format("{0:0.0}", expertises[index].Tau1 *1e9);
            delTwoData.Text = string.Format("{0:0.0}", expertises[index].Tau2 *1e9);
            delThreeData.Text = string.Format("{0:0.0}", expertises[index].Tau3 *1e9);

            delIndData.Text = index.ToString();
            if (loadFile.Expertises[ExpIndex].Corr1_1 != null) DrawGraphManual(expertises[ExpIndex].Corr1_1, 0);
            if (loadFile.Expertises[ExpIndex].Corr2_1 != null) DrawGraphManual(expertises[ExpIndex].Corr2_1, 1);
            if (loadFile.Expertises[ExpIndex].Corr3_1 != null) DrawGraphManual(expertises[ExpIndex].Corr3_1, 2);
            if (loadFile.Expertises[ExpIndex].Corr4_1 != null) DrawGraphManual(expertises[ExpIndex].Corr4_1, 3);
        }

        #region YamlConstruction
        public class ReverseExpertise
        {
            public Expertise[] Expertises { get; set; }

            public ReverseExpertise()
            {
                Expertises = new Expertise[0];
            }

            public ReverseExpertise(List<Expertise> expertises)
            {
                Expertises = expertises.ToArray();
            }
        }

        public class Expertise 
        {
            public DateTime Time { get; set; }

            public float FreqkHz { get; set; }
            public float BandwidthkHz { get; set; }

            public double Tau1 { get; set; }
            public double Tau2 { get; set; }
            public double Tau3 { get; set; }

            public double Lat { get; set; }
            public double Lon { get; set; }
            public double Alt { get; set; }

            public double[] Corr1_1 { get; set; }
            public double[] Corr2_1 { get; set; }
            public double[] Corr3_1 { get; set; }
            public double[] Corr4_1 { get; set; }

            public Expertise() 
            {
                Time = DateTime.Now;

                FreqkHz = 0;
                BandwidthkHz = 0;

                Tau1 = 0;
                Tau2 = 0;
                Tau3 = 0;

                Lat = 0;
                Lon = 0;
                Alt = 0;

                Corr1_1 = new double[0];
                Corr2_1 = new double[0];
                Corr3_1 = new double[0];
                Corr4_1 = new double[0];
            }
        }

        public T YamlLoad<T>(string NameDotYaml) where T : new() 
        {
            string text = "";
            try
            {
                using (StreamReader sr = new StreamReader(NameDotYaml, Encoding.Default))
                {
                    text = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            var deserializer = new DeserializerBuilder().Build();

            var t = new T();
            try
            {
                t = deserializer.Deserialize<T>(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return t;
        }
        #endregion
    }
}
