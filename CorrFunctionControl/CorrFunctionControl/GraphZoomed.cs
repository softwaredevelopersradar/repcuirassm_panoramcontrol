﻿using Arction.Wpf.SemibindableCharting.Views.ViewXY;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace CorrFunctionControl
{
    public partial class CorrFunctionGraph
    {


        System.Windows.Point posDownOne;
        System.Windows.Point posUpOne;
        System.Windows.Point posDownTwo;
        System.Windows.Point posUpTwo;

        private void xAx1_RangeChanged(object sender, Arction.Wpf.SemibindableCharting.Axes.RangeChangedEventArgs e)
        {
            try
            {
                var aXysX = xAx1;

                if (e.NewMax - e.NewMin > XAxesCorrGraphSetMax - XAxesCorrGraphSetMin)
                {
                    aXysX.SetRange(XAxesCorrGraphSetMin, XAxesCorrGraphSetMax);
                    return;
                }
                if (e.NewMin < XAxesCorrGraphSetMin)
                {
                    double shift = XAxesCorrGraphSetMin - e.NewMin;
                    aXysX.SetRange(e.NewMin + shift, e.NewMax + shift);
                }
                if (e.NewMax > XAxesCorrGraphSetMax)
                {
                    double shift = e.NewMax - XAxesCorrGraphSetMax;
                    aXysX.SetRange(e.NewMin - shift, e.NewMax - shift);
                }
            }
            catch { }
        }

        private void yAx1_RangeChanged(object sender, Arction.Wpf.SemibindableCharting.Axes.RangeChangedEventArgs e) 
        {
            try
            {
                var aXysY = yAx1;
                if (e.NewMax - e.NewMin > YLengthMax - YLengthMin)
                {
                    aXysY.SetRange(YLengthMin, YLengthMax);
                    return;
                }
                if (e.NewMax > YLengthMax)
                {
                    aXysY.SetRange(YLengthMin, YLengthMax);
                }
                if (e.NewMin < YLengthMin)
                {
                    aXysY.SetRange(YLengthMin, YLengthMax);
                }
            }
            catch { }
        }

        private void xAx2_RangeChanged(object sender, Arction.Wpf.SemibindableCharting.Axes.RangeChangedEventArgs e)
        {
            try
            {
                var aXysX = xAx2;

                if (e.NewMax - e.NewMin > XAxesCorrGraphSetMax - XAxesCorrGraphSetMin)
                {
                    aXysX.SetRange(XAxesCorrGraphSetMin, XAxesCorrGraphSetMax);
                    return;
                }
                if (e.NewMin < XAxesCorrGraphSetMin)
                {
                    double shift = XAxesCorrGraphSetMin - e.NewMin;
                    aXysX.SetRange(e.NewMin + shift, e.NewMax + shift);
                }
                if (e.NewMax > XAxesCorrGraphSetMax)
                {
                    double shift = e.NewMax - XAxesCorrGraphSetMax;
                    aXysX.SetRange(e.NewMin - shift, e.NewMax - shift);
                }
            }
            catch { }
        }

        private void yAx2_RangeChanged(object sender, Arction.Wpf.SemibindableCharting.Axes.RangeChangedEventArgs e)
        {
            try
            {
                var aXysY = yAx2;
                if (e.NewMax - e.NewMin > YLengthMax - YLengthMin)
                {
                    aXysY.SetRange(YLengthMin, YLengthMax);
                    return;
                }
                if (e.NewMax > YLengthMax)
                {
                    aXysY.SetRange(YLengthMin, YLengthMax);
                }
                if (e.NewMin < YLengthMin)
                {
                    aXysY.SetRange(YLengthMin, YLengthMax);
                }
            }
            catch { }
        }

        private void ViewXY_Zoomed(object sender, ZoomedXYEventArgs e)
        {
            ViewXY view = GraphTwo.ViewXY;

            double currentMin = view.XAxes[0].Minimum;
            double currentMax = view.XAxes[0].Maximum;

            GraphTwo.BeginUpdate();
            view.XAxes[0].SetRange(currentMin, currentMax);
            GraphTwo.EndUpdate();

            GraphTwo.BeginUpdate();
            view.XAxes[0].SetRange(currentMin, currentMax);
            GraphTwo.EndUpdate();

            GraphTwo.BeginUpdate();
            view.XAxes[0].SetRange(currentMin, currentMax);
            GraphTwo.EndUpdate();
        }

        private void ViewOne_Zoomed(object sender, ZoomedXYEventArgs e)
        {
            ViewXY view = GrapfOne.ViewXY;

            double currentMin = view.XAxes[0].Minimum;
            double currentMax = view.XAxes[0].Maximum;

            GrapfOne.BeginUpdate();
            view.XAxes[0].SetRange(currentMin, currentMax);
            GrapfOne.EndUpdate();

            GrapfOne.BeginUpdate();
            view.XAxes[0].SetRange(currentMin, currentMax);
            GrapfOne.EndUpdate();

            GrapfOne.BeginUpdate();
            view.XAxes[0].SetRange(currentMin, currentMax);
            GrapfOne.EndUpdate();
        }

        private void GrapfOne_MouseDown(object sender, MouseButtonEventArgs e)
        {
            posDownOne = e.GetPosition(GrapfOne);
        }

        private void GrapfOne_MouseUp(object sender, MouseButtonEventArgs e)
        {
            var cursPos = e.GetPosition(GrapfOne);
            var marginsRect = GrapfOne.ViewXY.GetMarginsRect();

            if ((cursPos.X >= marginsRect.X) && (cursPos.X <= marginsRect.X + marginsRect.Width) && (cursPos.Y >= marginsRect.Y) && (cursPos.Y <= marginsRect.Y + marginsRect.Height)) 
            {
                posUpOne = e.GetPosition(GrapfOne);

                if(posUpDown(posUpOne, posDownOne)) 
                {
                    var position = e.GetPosition(GrapfOne);
                    xAx1.CoordToValue((int)position.X, out var xValue, true);
                    yAx1.CoordToValue((int)position.Y, out var yValue, true);

                    CursorXOne.ValueAtXAxis = xValue;                    
                    CursorYOne.Value = yValue;
                    lT1.Text = string.Format("{0:0.00}", xValue) + " ns";

                    double width = Math.Abs(CursorXTwo.ValueAtXAxis - CursorXOne.ValueAtXAxis);
                    delFourData.Text = string.Format("{0:0.00}", width);
                }
            }
        }

        private void GraphTwo_MouseDown(object sender, MouseButtonEventArgs e) 
        {
            posDownTwo = e.GetPosition(GraphTwo);
        }

        private void GraphTwo_MouseUp(object sender, MouseButtonEventArgs e) 
        {
            var cursPos = e.GetPosition(GraphTwo);
            var marginsRect = GraphTwo.ViewXY.GetMarginsRect();
            if ((cursPos.X >= marginsRect.X) && (cursPos.X <= marginsRect.X + marginsRect.Width) && (cursPos.Y >= marginsRect.Y) && (cursPos.Y <= marginsRect.Y + marginsRect.Height))
            {
                posUpTwo = e.GetPosition(GraphTwo);

                if (posUpDown(posUpTwo, posDownTwo))
                {
                    var position = e.GetPosition(GraphTwo);
                    xAx2.CoordToValue((int)position.X, out var xValue, true);
                    yAx2.CoordToValue((int)position.Y, out var yValue, true);

                    CursorXOneM.ValueAtXAxis = xValue;
                    CursorYOneM.Value = yValue;
                    lT2.Content = string.Format("{0:0.00}", xValue) + " ns";
                }
            }
        }

        private bool posUpDown(System.Windows.Point posUp, System.Windows.Point posDown)
        {
            if (posDown == posUp)
                return true;

            if ((posDown.X - 1 <= posUp.X) && (posDown.X + 1 >= posUp.X) && (posDown.Y - 1 <= posUp.Y) && (posDown.Y + 1 >= posUp.Y))
                return true;
            else return false;
        }


        private void CursorXTwo_MovedByMouse(object sender, MouseEventArgs e)
        {            
            var position = e.GetPosition(GrapfOne);
            xAx1.CoordToValue((int)position.X, out var xValue, true);          

            CursorXTwo.ValueAtXAxis = xValue;

            double width = Math.Abs(CursorXTwo.ValueAtXAxis - CursorXOne.ValueAtXAxis);
            delFourData.Text = string.Format("{0:0.00}", width);
        }
    }
}
