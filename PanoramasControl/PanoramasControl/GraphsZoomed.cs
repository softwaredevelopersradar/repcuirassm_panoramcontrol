﻿using Arction.Wpf.SemibindableCharting.Views.ViewXY;
using System.Windows.Controls;

namespace PanoramasControl
{
    public partial class PanoramasGraph : UserControl
    {
        double YLengthMin = -120;
        double YLengthMax = 0;
        double XLengthMin = 10;
        double XLengthMax = 6010;

        #region Zoom
        private void Graph1_ViewXY_Zoomed(object sender, ZoomedXYEventArgs e)
        {
            ViewXY view = PanoramaOne.ViewXY;

            double currentMin = view.XAxes[0].Minimum;
            double currentMax = view.XAxes[0].Maximum;

            if (XLengthMin > currentMin || XLengthMax < currentMax)
            {
                if (XLengthMin > currentMin)
                    currentMin = XLengthMin;
                if (XLengthMax < currentMax)
                    currentMax = XLengthMax;

                PanoramaOne.BeginUpdate();
                view.XAxes[0].SetRange(currentMin, currentMax);
                PanoramaOne.EndUpdate();
            }

            //Frequency_1 = (currentMax - currentMin) / 2.0 + currentMin;
            //RangeFrq_1 = currentMax - currentMin;
        }

        private void Graph2_ViewXY_Zoomed(object sender, ZoomedXYEventArgs e)
        {
            ViewXY view = PanoramaTwo.ViewXY;

            double currentMin = view.XAxes[0].Minimum;
            double currentMax = view.XAxes[0].Maximum;

            if (XLengthMin > currentMin || XLengthMax < currentMax)
            {
                if (XLengthMin > currentMin)
                    currentMin = XLengthMin;
                if (XLengthMax < currentMax)
                    currentMax = XLengthMax;

                PanoramaTwo.BeginUpdate();
                view.XAxes[0].SetRange(currentMin, currentMax);
                PanoramaTwo.EndUpdate();
            }

            //Frequency_2 = (currentMax - currentMin) / 2.0 + currentMin;
            //RangeFrq_2 = currentMax - currentMin;
        }

        private void Graph3_ViewXY_Zoomed(object sender, ZoomedXYEventArgs e)
        {
            ViewXY view = PanoramaThree.ViewXY;

            double currentMin = view.XAxes[0].Minimum;
            double currentMax = view.XAxes[0].Maximum;

            if (XLengthMin > currentMin || XLengthMax < currentMax)
            {
                if (XLengthMin > currentMin)
                    currentMin = XLengthMin;
                if (XLengthMax < currentMax)
                    currentMax = XLengthMax;

                PanoramaThree.BeginUpdate();
                view.XAxes[0].SetRange(currentMin, currentMax);
                PanoramaThree.EndUpdate();
            }

            //Frequency_3 = (currentMax - currentMin) / 2.0 + currentMin;
            //RangeFrq_3 = currentMax - currentMin;
        }

        private void Graph4_ViewXY_Zoomed(object sender, ZoomedXYEventArgs e)
        {
            ViewXY view = PanoramaFour.ViewXY;

            double currentMin = view.XAxes[0].Minimum;
            double currentMax = view.XAxes[0].Maximum;

            if (XLengthMin > currentMin || XLengthMax < currentMax)
            {
                if (XLengthMin > currentMin)
                    currentMin = XLengthMin;
                if (XLengthMax < currentMax)
                    currentMax = XLengthMax;

                PanoramaFour.BeginUpdate();
                view.XAxes[0].SetRange(currentMin, currentMax);
                PanoramaFour.EndUpdate();
            }

            //Frequency_4 = (currentMax - currentMin) / 2.0 + currentMin;
            //RangeFrq_4 = currentMax - currentMin;
        }

        private void Graph5_ViewXY_Zoomed(object sender, ZoomedXYEventArgs e)
        {
            ViewXY view = PanoramaFive.ViewXY;

            double currentMin = view.XAxes[0].Minimum;
            double currentMax = view.XAxes[0].Maximum;

            if (XLengthMin > currentMin || XLengthMax < currentMax)
            {
                if (XLengthMin > currentMin)
                    currentMin = XLengthMin;
                if (XLengthMax < currentMax)
                    currentMax = XLengthMax;

                PanoramaFive.BeginUpdate();
                view.XAxes[0].SetRange(currentMin, currentMax);
                PanoramaFive.EndUpdate();
            }

            //Frequency_5 = (currentMax - currentMin) / 2.0 + currentMin;
            //RangeFrq_5 = currentMax - currentMin;
        }
        #endregion

        #region XYRangeChanged
        private void AxisY1_RangeChanged(object sender, Arction.Wpf.SemibindableCharting.Axes.RangeChangedEventArgs e)
        {
            try
            {
                var aXysY = YAxesOne;                
                if (e.NewMax - e.NewMin > MaxAmplitudeYAxe - YLengthMin)
                {
                    aXysY.SetRange(YLengthMin, MaxAmplitudeYAxe);
                    return;
                }
                if (e.NewMax > MaxAmplitudeYAxe)
                {                    
                    aXysY.SetRange(YLengthMin, MaxAmplitudeYAxe);
                }
                if (e.NewMin < YLengthMin)
                {                    
                    aXysY.SetRange(YLengthMin, MaxAmplitudeYAxe);
                }                
            }
            catch { }
        }

        private void AxisX1_RangeChanged(object sender, Arction.Wpf.SemibindableCharting.Axes.RangeChangedEventArgs e)
        {
            var aXysX = XAxesOne;
            if (e.NewMax - e.NewMin > XLengthMax - XLengthMin)
            {
                aXysX.SetRange(XLengthMin, XLengthMax);
                return;
            }
            if (e.NewMin < XLengthMin)
            {
                double shift = XLengthMin - e.NewMin;

                aXysX.SetRange(e.NewMin + shift, e.NewMax + shift);
            }
            if (e.NewMax > XLengthMax)
            {
                double shift = e.NewMax - XLengthMax;
                aXysX.SetRange(e.NewMin - shift, e.NewMax - shift);
            }
        }

        private void AxisY2_RangeChanged(object sender, Arction.Wpf.SemibindableCharting.Axes.RangeChangedEventArgs e)
        {
            try
            {
                var aXys = YAxesTwo;
                if (e.NewMax - e.NewMin > MaxAmplitudeYAxe - YLengthMin)
                {
                    aXys.SetRange(YLengthMin, MaxAmplitudeYAxe);
                    return;
                }
                if (e.NewMax > MaxAmplitudeYAxe)
                {
                    aXys.SetRange(YLengthMin, MaxAmplitudeYAxe);
                }
                if (e.NewMin < YLengthMin)
                {
                    aXys.SetRange(YLengthMin, MaxAmplitudeYAxe);
                }
            }
            catch { }
        }

        private void AxisX2_RangeChanged(object sender, Arction.Wpf.SemibindableCharting.Axes.RangeChangedEventArgs e)
        {
            var aXysX = XAxesTwo;
            if (e.NewMax - e.NewMin > XLengthMax - XLengthMin)
            {
                aXysX.SetRange(XLengthMin, XLengthMax);
                return;
            }
            if (e.NewMin < XLengthMin)
            {
                double shift = XLengthMin - e.NewMin;

                aXysX.SetRange(e.NewMin + shift, e.NewMax + shift);
            }
            if (e.NewMax > XLengthMax)
            {
                double shift = e.NewMax - XLengthMax;
                aXysX.SetRange(e.NewMin - shift, e.NewMax - shift);
            }
        }

        private void AxisY3_RangeChanged(object sender, Arction.Wpf.SemibindableCharting.Axes.RangeChangedEventArgs e)
        {
            try
            {
                var aXys = YAxesThree;
                if (e.NewMax - e.NewMin > MaxAmplitudeYAxe - YLengthMin)
                {
                    aXys.SetRange(YLengthMin, MaxAmplitudeYAxe);
                    return;
                }
                if (e.NewMax > MaxAmplitudeYAxe)
                {
                    aXys.SetRange(YLengthMin, MaxAmplitudeYAxe);
                }
                if (e.NewMin < YLengthMin)
                {
                    aXys.SetRange(YLengthMin, MaxAmplitudeYAxe);
                }
            }
            catch { }
        }

        private void AxisX3_RangeChanged(object sender, Arction.Wpf.SemibindableCharting.Axes.RangeChangedEventArgs e)
        {
            var aXysX = XAxesThree;
            if (e.NewMax - e.NewMin > XLengthMax - XLengthMin)
            {
                aXysX.SetRange(XLengthMin, XLengthMax);
                return;
            }
            if (e.NewMin < XLengthMin)
            {
                double shift = XLengthMin - e.NewMin;

                aXysX.SetRange(e.NewMin + shift, e.NewMax + shift);
            }
            if (e.NewMax > XLengthMax)
            {
                double shift = e.NewMax - XLengthMax;
                aXysX.SetRange(e.NewMin - shift, e.NewMax - shift);
            }
        }

        private void AxisY4_RangeChanged(object sender, Arction.Wpf.SemibindableCharting.Axes.RangeChangedEventArgs e)
        {
            try
            {
                var aXys = YAxesFour;
                if (e.NewMax - e.NewMin > MaxAmplitudeYAxe - YLengthMin)
                {
                    aXys.SetRange(YLengthMin, MaxAmplitudeYAxe);
                    return;
                }
                if (e.NewMax > MaxAmplitudeYAxe)
                {
                    aXys.SetRange(YLengthMin, MaxAmplitudeYAxe);
                }
                if (e.NewMin < YLengthMin)
                {
                    aXys.SetRange(YLengthMin, MaxAmplitudeYAxe);
                }
            }
            catch { }
        }

        private void AxisX4_RangeChanged(object sender, Arction.Wpf.SemibindableCharting.Axes.RangeChangedEventArgs e)
        {
            var aXysX = XAxesFour;
            if (e.NewMax - e.NewMin > XLengthMax - XLengthMin)
            {
                aXysX.SetRange(XLengthMin, XLengthMax);
                return;
            }
            if (e.NewMin < XLengthMin)
            {
                double shift = XLengthMin - e.NewMin;

                aXysX.SetRange(e.NewMin + shift, e.NewMax + shift);
            }
            if (e.NewMax > XLengthMax)
            {
                double shift = e.NewMax - XLengthMax;
                aXysX.SetRange(e.NewMin - shift, e.NewMax - shift);
            }
        }

        private void AxisY5_RangeChanged(object sender, Arction.Wpf.SemibindableCharting.Axes.RangeChangedEventArgs e)
        {
            try
            {
                var aXys = YAxesFive;
                if (e.NewMax - e.NewMin > MaxAmplitudeYAxe - YLengthMin)
                {
                    aXys.SetRange(YLengthMin, MaxAmplitudeYAxe);
                    return;
                }
                if (e.NewMax > MaxAmplitudeYAxe)
                {
                    aXys.SetRange(YLengthMin, MaxAmplitudeYAxe);
                }
                if (e.NewMin < YLengthMin)
                {
                    aXys.SetRange(YLengthMin, MaxAmplitudeYAxe);
                }
            }
            catch { }
        }

        private void AxisX5_RangeChanged(object sender, Arction.Wpf.SemibindableCharting.Axes.RangeChangedEventArgs e)
        {
            var aXysX = XAxesFive;
            if (e.NewMax - e.NewMin > XLengthMax - XLengthMin)
            {
                aXysX.SetRange(XLengthMin, XLengthMax);
                return;
            }
            if (e.NewMin < XLengthMin)
            {
                double shift = XLengthMin - e.NewMin;

                aXysX.SetRange(e.NewMin + shift, e.NewMax + shift);
            }
            if (e.NewMax > XLengthMax)
            {
                double shift = e.NewMax - XLengthMax;
                aXysX.SetRange(e.NewMin - shift, e.NewMax - shift);
            }
        }
        #endregion
    }
}
