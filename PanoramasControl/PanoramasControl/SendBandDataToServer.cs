﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanoramasControl
{
    public partial class PanoramasGraph
    {
        public delegate void FreqTypeAndTwoDoubleEventHandler(FrequencyType frequencyType, double startFreq, double endFreq, int idPanoram);
        public event FreqTypeAndTwoDoubleEventHandler OnFreqArea;

        public enum FrequencyType
        {
            Forbidden = 0,
            Known = 1,
            Important = 2,
            Signal = 3,
            Target = 4,
            Recognition = 5,
            RecognitionM = 6,
            RecognitionL = 7
        }

        private void InitBands()
        {
            BandFreq1.Visible = false;
            BandFreq2.Visible = false;
            BandFreq3.Visible = false;
            BandFreq4.Visible = false;
            BandFreq5.Visible = false;
        }

        #region Buttons Signal And Target
        private void BSignal1_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if(BandFreq1.Visible == true)
            {
                OnFreqArea?.Invoke(FrequencyType.Signal, BandFreq1.ValueBegin, BandFreq1.ValueEnd, 1);
            }
        }

        private void BTarget1_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (BandFreq1.Visible == true)
            {
                OnFreqArea?.Invoke(FrequencyType.Target, BandFreq1.ValueBegin, BandFreq1.ValueEnd, 1);
            }
        }

        private void BSignal2_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (BandFreq2.Visible == true)
            {
                OnFreqArea?.Invoke(FrequencyType.Signal, BandFreq2.ValueBegin, BandFreq2.ValueEnd, 2);
            }
        }

        private void BTarget2_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (BandFreq2.Visible == true)
            {
                OnFreqArea?.Invoke(FrequencyType.Target, BandFreq2.ValueBegin, BandFreq2.ValueEnd, 2);
            }
        }

        private void BSignal3_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (BandFreq3.Visible == true)
            {
                OnFreqArea?.Invoke(FrequencyType.Signal, BandFreq3.ValueBegin, BandFreq3.ValueEnd, 3);
            }
        }

        private void BTarget3_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (BandFreq3.Visible == true)
            {
                OnFreqArea?.Invoke(FrequencyType.Target, BandFreq3.ValueBegin, BandFreq3.ValueEnd, 3);
            }
        }

        private void BSignal4_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (BandFreq4.Visible == true)
            {
                OnFreqArea?.Invoke(FrequencyType.Signal, BandFreq4.ValueBegin, BandFreq4.ValueEnd, 4);
            }
        }

        private void BTarget4_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (BandFreq4.Visible == true)
            {
                OnFreqArea?.Invoke(FrequencyType.Target, BandFreq4.ValueBegin, BandFreq4.ValueEnd, 0);
            }
        }

        private void BSignal5_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (BandFreq5.Visible == true)
            {
                OnFreqArea?.Invoke(FrequencyType.Signal, BandFreq5.ValueBegin, BandFreq5.ValueEnd, 0);
            }
        }

        private void BTarget5_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (BandFreq5.Visible == true)
            {
                OnFreqArea?.Invoke(FrequencyType.Target, BandFreq5.ValueBegin, BandFreq5.ValueEnd, 0);
            }
        }
        #endregion

        #region MouseDoubleClick
        private void PanoramaOne_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var pos = e.GetPosition(PanoramaOne);
            var view = PanoramaOne.ViewXY;
            XAxesOne.CoordToValue((int)pos.X, out var xValue, true);

            if(BandFreq1.Visible == false)
            {
                BandFreq1.Visible = true;
                BandFreq1.ValueBegin = xValue - (xValue - view.XAxes[0].Minimum) / 4f;
                BandFreq1.ValueEnd = xValue + (view.XAxes[0].Maximum - xValue) / 4f;
            }
            else { BandFreq1.Visible = false; }
        }

        private void PanoramaTwo_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var pos = e.GetPosition(PanoramaTwo);
            var view = PanoramaTwo.ViewXY;
            XAxesTwo.CoordToValue((int)pos.X, out var xValue, true);

            if (BandFreq2.Visible == false)
            {
                BandFreq2.Visible = true;
                BandFreq2.ValueBegin = xValue - (xValue - view.XAxes[0].Minimum) / 4f;
                BandFreq2.ValueEnd = xValue + (view.XAxes[0].Maximum - xValue) / 4f;
            }
            else { BandFreq2.Visible = false; }
        }

        private void PanoramaThree_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var pos = e.GetPosition(PanoramaThree);
            var view = PanoramaThree.ViewXY;
            XAxesThree.CoordToValue((int)pos.X, out var xValue, true);

            if (BandFreq3.Visible == false)
            {
                BandFreq3.Visible = true;
                BandFreq3.ValueBegin = xValue - (xValue - view.XAxes[0].Minimum) / 4f;
                BandFreq3.ValueEnd = xValue + (view.XAxes[0].Maximum - xValue) / 4f;
            }
            else { BandFreq3.Visible = false; }
        }

        private void PanoramaFour_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var pos = e.GetPosition(PanoramaFour);
            var view = PanoramaFour.ViewXY;
            XAxesFour.CoordToValue((int)pos.X, out var xValue, true);

            if (BandFreq4.Visible == false)
            {
                BandFreq4.Visible = true;
                BandFreq4.ValueBegin = xValue - (xValue - view.XAxes[0].Minimum) / 4f;
                BandFreq4.ValueEnd = xValue + (view.XAxes[0].Maximum - xValue) / 4f;
            }
            else { BandFreq4.Visible = false; }
        }

        private void PanoramaFive_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var pos = e.GetPosition(PanoramaFive);
            var view = PanoramaFive.ViewXY;
            XAxesFive.CoordToValue((int)pos.X, out var xValue, true);

            if (BandFreq5.Visible == false)
            {
                BandFreq5.Visible = true;
                BandFreq5.ValueBegin = xValue - (xValue - view.XAxes[0].Minimum) / 4f;
                BandFreq5.ValueEnd = xValue + (view.XAxes[0].Maximum - xValue) / 4f;
            }
            else { BandFreq5.Visible = false; }
        }
        #endregion
    }
}
