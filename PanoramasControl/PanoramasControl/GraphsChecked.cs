﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace PanoramasControl
{
    public partial class PanoramasGraph : UserControl
    {
        Point posDown1;
        Point posUp1;
        Point posDown2;
        Point posUp2;
        Point posDown3;
        Point posUp3;
        Point posDown4;
        Point posUp4;
        Point posDown5;
        Point posUp5;
        Color colorDefault = Color.FromRgb(89, 88, 88);
        Color colorCheck = Color.FromRgb(100, 1, 20);
        byte indGraph = 1;
        public byte IndGraph 
        {
            get => indGraph;
            set 
            {
                indGraph = value;
                switch (indGraph) 
                {
                    case 1:
                        Fill1.GradientColor = colorCheck;
                        Fill2.GradientColor = colorDefault;
                        Fill3.GradientColor = colorDefault;
                        Fill4.GradientColor = colorDefault;
                        Fill5.GradientColor = colorDefault;
                        break;
                    case 2:
                        Fill1.GradientColor = colorDefault;
                        Fill2.GradientColor = colorCheck;
                        Fill3.GradientColor = colorDefault;
                        Fill4.GradientColor = colorDefault;
                        Fill5.GradientColor = colorDefault;
                        break;
                    case 3:
                        Fill1.GradientColor = colorDefault;
                        Fill2.GradientColor = colorDefault;
                        Fill3.GradientColor = colorCheck;
                        Fill4.GradientColor = colorDefault;
                        Fill5.GradientColor = colorDefault;
                        break;
                    case 4:
                        Fill1.GradientColor = colorDefault;
                        Fill2.GradientColor = colorDefault;
                        Fill3.GradientColor = colorDefault;
                        Fill4.GradientColor = colorCheck;
                        Fill5.GradientColor = colorDefault;
                        break;
                    case 5:
                        Fill1.GradientColor = colorDefault;
                        Fill2.GradientColor = colorDefault;
                        Fill3.GradientColor = colorDefault;
                        Fill4.GradientColor = colorDefault;
                        Fill5.GradientColor = colorCheck;
                        break;
                    default:
                        Fill1.GradientColor = colorCheck;
                        Fill2.GradientColor = colorDefault;
                        Fill3.GradientColor = colorDefault;
                        Fill4.GradientColor = colorDefault;
                        Fill5.GradientColor = colorDefault;
                        break;
                }
            }
        }

        public delegate void CheckedGraph(object sender, byte index);
        public event CheckedGraph OnCheckGraph = (sender, index) => { };
        private void PanoramaOne_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            posDown1 = e.GetPosition(PanoramaOne);
        }

        private void PanoramaOne_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e) 
        {
            IndGraph = 1;
            OnCheckGraph?.Invoke(this, indGraph);
        }

        private void PanoramaTwo_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            posDown2 = e.GetPosition(PanoramaTwo);
        }

        private void PanoramaTwo_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            IndGraph = 2;
            OnCheckGraph?.Invoke(this, indGraph);
        }

        private void PanoramaThree_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            posDown3 = e.GetPosition(PanoramaThree);
        }

        private void PanoramaThree_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            IndGraph = 3;
            OnCheckGraph?.Invoke(this, indGraph);
        }

        private void PanoramaFour_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            posDown4 = e.GetPosition(PanoramaFour);
        }

        private void PanoramaFour_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            IndGraph = 4;
            OnCheckGraph?.Invoke(this, indGraph);
        }

        private void PanoramaFive_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            posDown5 = e.GetPosition(PanoramaFive);
        }

        private void PanoramaFive_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            IndGraph = 5;
            OnCheckGraph?.Invoke(this, indGraph);
        }
    }
}
