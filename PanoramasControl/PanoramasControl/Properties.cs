﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace PanoramasControl
{
    public partial class PanoramasGraph : UserControl
    {
        int maxAmplitudeYAxe = -120;
        public int MaxAmplitudeYAxe 
        {
            get => maxAmplitudeYAxe;
            set
            {
                maxAmplitudeYAxe = value;
                UpdateYAxes(value);
            }
        }

        private void UpdateYAxes(int value)
        {
            PanoramaOne.ViewXY.YAxes[0].Maximum = value;
            PanoramaTwo.ViewXY.YAxes[0].Maximum = value;
            PanoramaThree.ViewXY.YAxes[0].Maximum = value;
            PanoramaFour.ViewXY.YAxes[0].Maximum = value;
            PanoramaFive.ViewXY.YAxes[0].Maximum = value;
        }

        double Freq_1 = 180.000;
        public double Frequency_1
        {
            get => Freq_1;
            set
            {
                Freq_1 = value;

                var _panorama = PanoramaOne.ViewXY;
                PanoramaOne.Dispatcher.Invoke(delegate ()
                {
                    _panorama.XAxes[0].Maximum = Freq_1 + Range_1 / 2.0;
                    _panorama.XAxes[0].Minimum = Freq_1 - Range_1 / 2.0;
                });
            }
        }

        double Range_1 = 90.000;
        public double RangeFrq_1
        {
            get => Range_1;
            set
            {
                Range_1 = value;

                var _panorama = PanoramaOne.ViewXY;
                PanoramaOne.Dispatcher.Invoke(delegate ()
                {
                    _panorama.XAxes[0].Maximum = Freq_1 + Range_1 / 2.0;
                    _panorama.XAxes[0].Minimum = Freq_1 - Range_1 / 2.0;
                });
            }
        }

        double Freq_2 = 2470;
        public double Frequency_2
        {
            get => Freq_2;
            set
            {
                Freq_2 = value;

                var _panorama = PanoramaTwo.ViewXY;
                PanoramaOne.Dispatcher.Invoke(delegate ()
                {
                    _panorama.XAxes[0].Maximum = Freq_2 + Range_2 / 2.0;
                    _panorama.XAxes[0].Minimum = Freq_2 - Range_2 / 2.0;
                });
            }
        }

        double Range_2 = 62.500;
        public double RangeFrq_2
        {
            get => Range_2;
            set
            {
                Range_2 = value;

                var _panorama = PanoramaTwo.ViewXY;
                PanoramaOne.Dispatcher.Invoke(delegate ()
                {
                    _panorama.XAxes[0].Maximum = Freq_2 + Range_2 / 2.0;
                    _panorama.XAxes[0].Minimum = Freq_2 - Range_2 / 2.0;
                });
            }
        }

        double Freq_3 = 1800;
        public double Frequency_3
        {
            get => Freq_3;
            set
            {
                Freq_3 = value;

                var _panorama = PanoramaThree.ViewXY;
                PanoramaOne.Dispatcher.Invoke(delegate ()
                {
                    _panorama.XAxes[0].Maximum = Freq_3 + Range_3 / 2.0;
                    _panorama.XAxes[0].Minimum = Freq_3 - Range_3 / 2.0;
                });
            }
        }

        double Range_3 = 62.500;
        public double RangeFrq_3
        {
            get => Range_3;
            set
            {
                Range_3 = value;

                var _panorama = PanoramaThree.ViewXY;
                PanoramaOne.Dispatcher.Invoke(delegate ()
                {
                    _panorama.XAxes[0].Maximum = Freq_3 + Range_3 / 2.0;
                    _panorama.XAxes[0].Minimum = Freq_3 - Range_3 / 2.0;
                });
            }
        }

        double Freq_4 = 1500;
        public double Frequency_4
        {
            get => Freq_4;
            set
            {
                Freq_4 = value;

                var _panorama = PanoramaFour.ViewXY;
                PanoramaOne.Dispatcher.Invoke(delegate ()
                {
                    _panorama.XAxes[0].Maximum = Freq_4 + Range_4 / 2.0;
                    _panorama.XAxes[0].Minimum = Freq_4 - Range_4 / 2.0;
                });
            }
        }

        double Range_4 = 62.500;
        public double RangeFrq_4
        {
            get => Range_4;
            set
            {
                Range_4 = value;
                              
                var _panorama = PanoramaFour.ViewXY;
                PanoramaOne.Dispatcher.Invoke(delegate ()
                {
                    _panorama.XAxes[0].Maximum = Freq_4 + Range_4 / 2.0;
                    _panorama.XAxes[0].Minimum = Freq_4 - Range_4 / 2.0;
                });
            }
        }

        double Freq_5 = 1200;
        public double Frequency_5
        {
            get => Freq_5;
            set
            {
                Freq_5 = value;                

                var _panorama = PanoramaFive.ViewXY;
                PanoramaOne.Dispatcher.Invoke(delegate ()
                {
                    _panorama.XAxes[0].Maximum = Freq_5 + Range_5 / 2.0;
                    _panorama.XAxes[0].Minimum = Freq_5 - Range_5 / 2.0;
                });
            }
        }

        double Range_5 = 62.500;
        public double RangeFrq_5
        {
            get => Range_5;
            set
            {
                Range_5 = value;                
                
                var _panorama = PanoramaFive.ViewXY;
                PanoramaOne.Dispatcher.Invoke(delegate ()
                {
                    _panorama.XAxes[0].Maximum = Freq_5 + Range_5 / 2.0;
                    _panorama.XAxes[0].Minimum = Freq_5 - Range_5 / 2.0;
                });
            }
        }

        
    }
}
