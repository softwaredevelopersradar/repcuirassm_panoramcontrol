﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace PanoramasControl
{
    public partial class PanoramasGraph : UserControl
    {
        public new void Language(string value)
        {
            //1. Создаём ResourceDictionary для новой культуры
            ResourceDictionary dict = new ResourceDictionary();
            switch (value)
            {
                case "RU":
                    dict.Source = new Uri(String.Format("/PanoramasControl;component/ResourcesLang/lang.ru-RU.xaml", value), UriKind.Relative);
                    break;
                case "EN":
                    dict.Source = new Uri(String.Format("/PanoramasControl;component/ResourcesLang/lang.en-US.xaml", value), UriKind.Relative);
                    break;
                default:
                    dict.Source = new Uri("/PanoramasControl;component/ResourcesLang/lang.en-US.xaml", UriKind.Relative);
                    break;
            }


            //2. Находим старую ResourceDictionary и удаляем его и добавляем новую ResourceDictionary
            ResourceDictionary oldDict = (from d in Resources.MergedDictionaries
                                          where d.Source != null && d.Source.OriginalString.StartsWith("/PanoramasControl;component/ResourcesLang/lang.")
                                          select d).First();

            if (oldDict != null)
            {
                int ind = Resources.MergedDictionaries.IndexOf(oldDict);
                Resources.MergedDictionaries.Remove(oldDict);
                Resources.MergedDictionaries.Insert(ind, dict);

            }
            else
            {
                Resources.MergedDictionaries.Add(dict);
            }
        }
    }
}
