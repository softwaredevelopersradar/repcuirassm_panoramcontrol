﻿using Arction.Wpf.SemibindableCharting;
using Nito.AsyncEx;
using SharpDX.Mathematics;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;


namespace PanoramasControl
{
    public partial class PanoramasGraph : UserControl
    {
        double[] ArrayPoints_1 = new double[3000];
        double[] ArrayPoints_2 = new double[3000];
        double[] ArrayPoints_3 = new double[3000];
        double[] ArrayPoints_4 = new double[3000];
        double[] ArrayPoints_5 = new double[3000];

        List<double> freqs;
        List<bool> isAddF;
        bool addF = true;
        double[] minF;
        double[] maxF;
        Int16 minX = 10;
        Int16 maxX = 6010;

        private int pointsCount = 3000;
        private int PointsCount 
        {
            get => pointsCount;
            set 
            {
                pointsCount = value;
            }
        }

        private byte cbIndexOne = 1;
        private byte CbIndexOne 
        {
            get => cbIndexOne;
            set 
            {
                cbIndexOne = value;
            }
        }
        private byte cbIndexTwo = 1;
        private byte CbIndexTwo
        {
            get => cbIndexTwo;
            set
            {
                cbIndexTwo = value;
            }
        }
        private byte cbIndexThree = 1;
        private byte CbIndexThree
        {
            get => cbIndexThree;
            set
            {
                cbIndexThree = value;
            }
        }
        private byte cbIndexFour = 1;
        private byte CbIndexFour
        {
            get => cbIndexFour;
            set
            {
                cbIndexFour = value;
            }
        }
        private byte cbIndexFive = 1;
        private byte CbIndexFive
        {
            get => cbIndexFive;
            set
            {
                cbIndexFive = value;
            }
        }

        AsyncAutoResetEvent _asyncSemaphore1 = new AsyncAutoResetEvent();
        AsyncAutoResetEvent _asyncSemaphore2 = new AsyncAutoResetEvent();
        AsyncAutoResetEvent _asyncSemaphore3 = new AsyncAutoResetEvent();
        AsyncAutoResetEvent _asyncSemaphore4 = new AsyncAutoResetEvent();
        AsyncAutoResetEvent _asyncSemaphore5 = new AsyncAutoResetEvent();

        private bool isSpectrumRequest = false;
        CancellationTokenSource cts = new CancellationTokenSource();
        private int _CancellationTokenDelay = 100;
        public int CancellationTokenDelay
        {
            get { return _CancellationTokenDelay; }
            set
            {
                if (_CancellationTokenDelay != value)
                {
                    _CancellationTokenDelay = value;
                }
            }
        }
        private int _RequestSpectrumTimer = 100;
        public int RequestSpectrumTimer
        {
            get { return _RequestSpectrumTimer; }
            set
            {
                if (_RequestSpectrumTimer != value)
                {
                    _RequestSpectrumTimer = value;
                }
            }
        }

        public delegate void RangeSpectrumXEventHandler(object sender, byte ID, double MinVisibleX, double MaxVisibleX, int PointCount);
        public event RangeSpectrumXEventHandler NeedSpectrumRequest = (sender, id, min, max, count) => { };
        public event RangeSpectrumXEventHandler NeedSpectrumRequest2 = (sender, id, min, max, count) => { };
        public event RangeSpectrumXEventHandler NeedSpectrumRequest3 = (sender, id, min, max, count) => { };
        public event RangeSpectrumXEventHandler NeedSpectrumRequest4 = (sender, id, min, max, count) => { };
        public event RangeSpectrumXEventHandler NeedSpectrumRequest5 = (sender, id, min, max, count) => { };

        private void TaskManager()
        {
            Task.Run(() =>SpectrumRequest());
        }

        private async void SpectrumRequest()
        {
            while (isSpectrumRequest)
            {
                cts = new CancellationTokenSource();
                CancellationToken token = cts.Token;
                try 
                {
                    DispatchIfNecessary(() =>
                    {
                        switch (countPanoramas)
                        {
                            case 1:
                                if (isAddF[0] == true)
                                {
                                    if ((Freq_1 - Range_1 / 2.0) > minX && (Freq_1 + Range_1 / 2.0) < maxX)
                                    {
                                        if(cbIndexOne != 8) 
                                        {
                                            NeedSpectrumRequest?.Invoke(this, cbIndexOne, Freq_1 - Range_1 / 2.0, Freq_1 + Range_1 / 2.0, 3000);
                                            _asyncSemaphore1.WaitAsync();
                                        }                                            
                                    }                                        
                                }
                                break;
                            case 2:
                                if (isAddF[0] == true) 
                                {
                                    if ((Freq_1 - Range_1 / 2.0) > minX && (Freq_1 + Range_1 / 2.0) < maxX)
                                    {
                                        if (cbIndexOne != 8) 
                                        {
                                            NeedSpectrumRequest?.Invoke(this, cbIndexOne, Freq_1 - Range_1 / 2.0, Freq_1 + Range_1 / 2.0, 3000);
                                            _asyncSemaphore1.WaitAsync();
                                        }                                            
                                    }
                                }
                                if (isAddF[1] == true)
                                {
                                    if ((Freq_2 - Range_2 / 2.0) > minX && (Freq_2 + Range_2 / 2.0) < maxX) 
                                    {
                                        if(cbIndexTwo != 8) 
                                        {
                                            NeedSpectrumRequest2?.Invoke(this, cbIndexTwo, Freq_2 - Range_2 / 2.0, Freq_2 + Range_2 / 2.0, 3000);
                                            _asyncSemaphore2.WaitAsync();
                                        }
                                    }                                        
                                }
                                break;
                            case 3:
                                if (isAddF[0] == true)
                                {
                                    if ((Freq_1 - Range_1 / 2.0) > minX && (Freq_1 + Range_1 / 2.0) < maxX)
                                    {
                                        if (cbIndexOne != 8) 
                                        {
                                            NeedSpectrumRequest?.Invoke(this, cbIndexOne, Freq_1 - Range_1 / 2.0, Freq_1 + Range_1 / 2.0, 3000);
                                            _asyncSemaphore1.WaitAsync();
                                        }
                                    }
                                }
                                if (isAddF[1] == true)
                                {
                                    if ((Freq_2 - Range_2 / 2.0) > minX && (Freq_2 + Range_2 / 2.0) < maxX)
                                    {
                                        if (cbIndexTwo != 8) 
                                        {
                                            NeedSpectrumRequest2?.Invoke(this, cbIndexTwo, Freq_2 - Range_2 / 2.0, Freq_2 + Range_2 / 2.0, 3000);
                                            _asyncSemaphore2.WaitAsync();
                                        }
                                    }
                                }
                                if (isAddF[2] == true)
                                {
                                    if ((Freq_3 - Range_3 / 2.0) > minX && (Freq_3 + Range_3 / 2.0) < maxX) 
                                    {
                                        if(cbIndexThree != 8) 
                                        {
                                            NeedSpectrumRequest3?.Invoke(this, cbIndexThree, Freq_3 - Range_3 / 2.0, Freq_3 + Range_3 / 2.0, 3000);
                                            _asyncSemaphore3.WaitAsync();
                                        }                                           
                                    }                                        
                                }
                                break;
                            case 4:
                                if (isAddF[0] == true)
                                {
                                    if ((Freq_1 - Range_1 / 2.0) > minX && (Freq_1 + Range_1 / 2.0) < maxX)
                                    {
                                        if (cbIndexOne != 8)
                                        {
                                            NeedSpectrumRequest?.Invoke(this, cbIndexOne, Freq_1 - Range_1 / 2.0, Freq_1 + Range_1 / 2.0, 3000);
                                            _asyncSemaphore1.WaitAsync();
                                        }
                                    }
                                }
                                if (isAddF[1] == true)
                                {
                                    if ((Freq_2 - Range_2 / 2.0) > minX && (Freq_2 + Range_2 / 2.0) < maxX)
                                    {
                                        if (cbIndexTwo != 8)
                                        {
                                            NeedSpectrumRequest2?.Invoke(this, cbIndexTwo, Freq_2 - Range_2 / 2.0, Freq_2 + Range_2 / 2.0, 3000);
                                            _asyncSemaphore2.WaitAsync();
                                        }
                                    }
                                }
                                if (isAddF[2] == true)
                                {
                                    if ((Freq_3 - Range_3 / 2.0) > minX && (Freq_3 + Range_3 / 2.0) < maxX)
                                    {
                                        if (cbIndexThree != 8)
                                        {
                                            NeedSpectrumRequest3?.Invoke(this, cbIndexThree, Freq_3 - Range_3 / 2.0, Freq_3 + Range_3 / 2.0, 3000);
                                            _asyncSemaphore3.WaitAsync();
                                        }
                                    }
                                }
                                if (isAddF[3] == true) 
                                {
                                    if ((Freq_4 - Range_4 / 2.0) > minX && (Freq_4 + Range_4 / 2.0) < maxX) 
                                    {
                                        if(cbIndexFour != 8) 
                                        {
                                            NeedSpectrumRequest4?.Invoke(this, cbIndexFour, Freq_4 - Range_4 / 2.0, Freq_4 + Range_4 / 2.0, 3000);
                                            _asyncSemaphore4.WaitAsync();
                                        }
                                    }                                        
                                }
                                break;
                            case 5:
                                if (isAddF[0] == true)
                                {
                                    if ((Freq_1 - Range_1 / 2.0) > minX && (Freq_1 + Range_1 / 2.0) < maxX)
                                    {
                                        if (cbIndexOne != 8)
                                        {
                                            NeedSpectrumRequest?.Invoke(this, cbIndexOne, Freq_1 - Range_1 / 2.0, Freq_1 + Range_1 / 2.0, 3000);
                                            _asyncSemaphore1.WaitAsync();
                                        }
                                    }
                                }
                                if (isAddF[1] == true)
                                {
                                    if ((Freq_2 - Range_2 / 2.0) > minX && (Freq_2 + Range_2 / 2.0) < maxX)
                                    {
                                        if (cbIndexTwo != 8)
                                        {
                                            NeedSpectrumRequest2?.Invoke(this, cbIndexTwo, Freq_2 - Range_2 / 2.0, Freq_2 + Range_2 / 2.0, 3000);
                                            _asyncSemaphore2.WaitAsync();
                                        }
                                    }
                                }
                                if (isAddF[2] == true)
                                {
                                    if ((Freq_3 - Range_3 / 2.0) > minX && (Freq_3 + Range_3 / 2.0) < maxX)
                                    {
                                        if (cbIndexThree != 8)
                                        {
                                            NeedSpectrumRequest3?.Invoke(this, cbIndexThree, Freq_3 - Range_3 / 2.0, Freq_3 + Range_3 / 2.0, 3000);
                                            _asyncSemaphore3.WaitAsync();
                                        }
                                    }
                                }
                                if (isAddF[3] == true)
                                {
                                    if ((Freq_4 - Range_4 / 2.0) > minX && (Freq_4 + Range_4 / 2.0) < maxX)
                                    {
                                        if (cbIndexFour != 8)
                                        {
                                            NeedSpectrumRequest4?.Invoke(this, cbIndexFour, Freq_4 - Range_4 / 2.0, Freq_4 + Range_4 / 2.0, 3000);
                                            _asyncSemaphore4.WaitAsync();
                                        }
                                    }
                                }
                                if (isAddF[4] == true) 
                                {
                                    if ((Freq_5 - Range_5 / 2.0) > minX && (Freq_5 + Range_5 / 2.0) < maxX) 
                                    {
                                        if (cbIndexFive != 8) 
                                        {
                                            NeedSpectrumRequest5?.Invoke(this, cbIndexFive, Freq_5 - Range_5 / 2.0, Freq_5 + Range_5 / 2.0, 3000);
                                            _asyncSemaphore5.WaitAsync();
                                        }                                            
                                    }
                                }
                                break;
                        }
                    });
                }
                catch { }
                
                try
                {
                    var T = await Task.Delay(_CancellationTokenDelay, token).ContinueWith(_ => { return 42; });
                }
                catch { }
                await Task.Delay(_RequestSpectrumTimer);
            }
        }

        public void UpdateMode(int Mode)
        {
            switch (Mode)
            {
                case 0:
                    isSpectrumRequest = false;
                    break;
                case 1:
                    isSpectrumRequest = true;
                    TaskManager();
                    break;
            }
        }

        #region ComboxIndexChanged for Channels
        private void InitComboBox() 
        {
            cbOne.SelectedIndex = 0;
            cbTwo.SelectedIndex = 0;
            cbThree.SelectedIndex = 0;
            cbFour.SelectedIndex = 0;
            cbFive.SelectedIndex = 0;
        }
        private void CbOne_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (cbOne.SelectedIndex) 
            {
                case 0: CbIndexOne = 1; break;
                case 1: CbIndexOne = 2; break;
                case 2: CbIndexOne = 3; break;
                case 3: CbIndexOne = 4; break;
                case 4: CbIndexOne = 5; break;
                case 5: CbIndexOne = 6; break;
                case 6: CbIndexOne = 7; break;
                case 7: CbIndexOne = 8; break;
                default: CbIndexOne = 1; break;
            }
        }
        private void CbTwo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (cbTwo.SelectedIndex)
            {
                case 0: CbIndexTwo = 1; break;
                case 1: CbIndexTwo = 2; break;
                case 2: CbIndexTwo = 3; break;
                case 3: CbIndexTwo = 4; break;
                case 4: CbIndexTwo = 5; break;
                case 5: CbIndexTwo = 6; break;
                case 6: CbIndexTwo = 7; break;
                case 7: CbIndexTwo = 8; break;
                default: CbIndexTwo = 1; break;
            }
        }
        private void CbThree_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (cbThree.SelectedIndex)
            {
                case 0: CbIndexThree = 1; break;
                case 1: CbIndexThree = 2; break;
                case 2: CbIndexThree = 3; break;
                case 3: CbIndexThree = 4; break;
                case 4: CbIndexThree = 5; break;
                case 5: CbIndexThree = 6; break;
                case 6: CbIndexThree = 7; break;
                case 7: CbIndexThree = 8; break;
                default: CbIndexThree = 1; break;
            }
        }
        private void CbFour_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (cbFour.SelectedIndex)
            {
                case 0: CbIndexFour = 1; break;
                case 1: CbIndexFour = 2; break;
                case 2: CbIndexFour = 3; break;
                case 3: CbIndexFour = 4; break;
                case 4: CbIndexFour = 5; break;
                case 5: CbIndexFour = 6; break;
                case 6: CbIndexFour = 7; break;
                case 7: CbIndexFour = 8; break;
                default: CbIndexFour = 1; break;
            }
        }
        private void CbFive_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (cbFive.SelectedIndex)
            {
                case 0: CbIndexFive = 1; break;
                case 1: CbIndexFive = 2; break;
                case 2: CbIndexFive = 3; break;
                case 3: CbIndexFive = 4; break;
                case 4: CbIndexFive = 5; break;
                case 5: CbIndexFive = 6; break;
                case 6: CbIndexFive = 7; break;
                case 7: CbIndexFive = 8; break;
                default: CbIndexFive = 1; break;
            }
        }
        #endregion

        public void Converter(float[] Points, byte ID, byte IDCh, double xMin, double xMax)
        {
            PointsCount = Points.Length;
            double[] dPoints = new double[pointsCount];

            for (int index = 0; index < pointsCount; index++)
            {
                dPoints[index] = Points[index];
            }

            switch (ID)
            {
                case 1:
                    DrawGraphsData(dPoints, IDCh, xMin, xMax);
                    break;
                case 2:
                    DrawGraphsData2(dPoints, IDCh, xMin, xMax);
                    break;
                case 3:
                    DrawGraphsDat3(dPoints, IDCh, xMin, xMax);
                    break;
                case 4:
                    DrawGraphsData4(dPoints, IDCh, xMin, xMax);
                    break;
                case 5:
                    DrawGraphsData5(dPoints, IDCh, xMin, xMax);
                    break;
            }            
        }

        // Check Frequency in Allowed Range
        public void CheckFrequency(double[] minFreqs, double[] maxFreqs) 
        {
            freqs = new List<double>() { Freq_1, Freq_2, Freq_3, Freq_4, Freq_5 };
            isAddF = new List<bool>() { false, false, false, false, false };
            int countF = minFreqs.Length;
            minF = new double[countF];
            maxF = new double[countF];
            minF = minFreqs;
            maxF = maxFreqs;
            int index = 0;

            foreach (double fr in freqs) 
            {
                for(int i=0; i < countF; i++) 
                {
                    if (fr > minFreqs[i] && fr < maxFreqs[i]) 
                    {
                        addF = true;
                        isAddF[index] = addF;                        
                    }
                    else 
                    {
                    }
                }
                index++;
            }
        }

        #region DrawGraphs

        private void InitArraySaveData()
        {
            for (int i = 0; i < pointsCount; i++)
            {
                ArrayPoints_1[i] = -120;
            }
            for (int i = 0; i < pointsCount; i++)
            {
                ArrayPoints_2[i] = -120;
            }
            for (int i = 0; i < pointsCount; i++)
            {
                ArrayPoints_3[i] = -120;
            }
            for (int i = 0; i < pointsCount; i++)
            {
                ArrayPoints_4[i] = -120;
            }
            for (int i = 0; i < pointsCount; i++)
            {
                ArrayPoints_5[i] = -120;
            }
        }

        private void DrawGraphsData(double[] Points, byte ID, double xMin, double xMax)
        {
            DispatchIfNecessary(() => 
            {
                var _view = PanoramaOne.ViewXY;

                int countPoint = Points.Length;
                var data = new SeriesPoint[countPoint];
                var dataSave = new SeriesPoint[countPoint];

                for (int i = 0; i < countPoint; i++)
                {
                    data[i].X = xMin + i * (xMax - xMin) / (countPoint - 1);
                    data[i].Y = Points[i];
                    dataSave[i].X = xMin + i * (xMax - xMin) / (countPoint - 1);

                    if (BSave.IsChecked == true)
                    {
                        _view.PointLineSeries[1].Visible = true;

                        if (ArrayPoints_1 != null)
                        {
                            if (ArrayPoints_1[i] < data[i].Y)
                            {
                                ArrayPoints_1[i] = data[i].Y;
                                dataSave[i].Y = ArrayPoints_1[i];
                            }
                            else { dataSave[i].Y = ArrayPoints_1[i]; }
                        }
                        else
                        {
                            ArrayPoints_1[i] = Points[i];
                            dataSave[i].Y = ArrayPoints_1[i];
                        }
                        _view.PointLineSeries[1].Visible = true;
                    }
                    else
                    {
                        _view.PointLineSeries[1].Visible = false;
                    }
                }
                
                switch (ID)
                {
                    case 1: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(255, 255, 0); break;
                    case 2: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(106, 176, 36); break;
                    case 3: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(68, 207, 194); break;
                    case 4: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(38, 100, 209); break;
                    case 5: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(251, 114, 0); break;
                    case 6: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(141, 41, 41); break;
                    case 7: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(255, 243, 0); break;
                }

                PanoramaOne.BeginUpdate();
                _view.PointLineSeries[0].Points = data;
                _view.PointLineSeries[1].Points = dataSave;
                _asyncSemaphore1.Set();
                PanoramaOne.EndUpdate();
            });
        }

        private void DrawGraphsData2(double[] Points, byte ID, double xMin, double xMax)
        {
            DispatchIfNecessary(() => 
            {
                var _view = PanoramaTwo.ViewXY;

                int countPoint = Points.Length;
                var data = new SeriesPoint[countPoint];
                var dataSave = new SeriesPoint[countPoint];

                for (int i = 0; i < countPoint; i++)
                {
                    data[i].X = xMin + i * (xMax - xMin) / (countPoint - 1);
                    data[i].Y = Points[i];
                    dataSave[i].X = xMin + i * (xMax - xMin) / (countPoint - 1);

                    if (BSave2.IsChecked == true)
                    {
                        _view.PointLineSeries[1].Visible = true;

                        if (ArrayPoints_2 != null)
                        {
                            if (ArrayPoints_2[i] < data[i].Y)
                            {
                                ArrayPoints_2[i] = data[i].Y;
                                dataSave[i].Y = ArrayPoints_2[i];
                            }
                            else { dataSave[i].Y = ArrayPoints_2[i]; }
                        }
                        else
                        {
                            ArrayPoints_2[i] = Points[i];
                            dataSave[i].Y = ArrayPoints_2[i];
                        }
                        _view.PointLineSeries[1].Visible = true;
                    }
                    else
                    {
                        _view.PointLineSeries[1].Visible = false;
                    }
                }

                switch (ID)
                {
                    case 1: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(255, 255, 0); break;
                    case 2: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(106, 176, 36); break;
                    case 3: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(68, 207, 194); break;
                    case 4: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(38, 100, 209); break;
                    case 5: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(251, 114, 0); break;
                    case 6: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(141, 41, 41); break;
                    case 7: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(255, 243, 0); break;
                }

                PanoramaTwo.BeginUpdate();
                _view.PointLineSeries[0].Points = data;
                _view.PointLineSeries[1].Points = dataSave;
                _asyncSemaphore2.Set();
                PanoramaTwo.EndUpdate();              
            });
        }

        private void DrawGraphsDat3(double[] Points, byte ID, double xMin, double xMax)
        {
            DispatchIfNecessary(() =>
            {
                var _view = PanoramaThree.ViewXY;

                int countPoint = Points.Length;
                var data = new SeriesPoint[countPoint];
                var dataSave = new SeriesPoint[countPoint];

                for (int i = 0; i < countPoint; i++)
                {
                    data[i].X = xMin + i * (xMax - xMin) / (countPoint - 1);
                    data[i].Y = Points[i];
                    dataSave[i].X = xMin + i * (xMax - xMin) / (countPoint - 1);

                    if (BSave3.IsChecked == true)
                    {
                        _view.PointLineSeries[1].Visible = true;

                        if (ArrayPoints_3 != null)
                        {
                            if (ArrayPoints_3[i] < data[i].Y)
                            {
                                ArrayPoints_3[i] = data[i].Y;
                                dataSave[i].Y = ArrayPoints_3[i];
                            }
                            else { dataSave[i].Y = ArrayPoints_3[i]; }
                        }
                        else
                        {
                            ArrayPoints_3[i] = Points[i];
                            dataSave[i].Y = ArrayPoints_3[i];
                        }
                        _view.PointLineSeries[1].Visible = true;
                    }
                    else
                    {
                        _view.PointLineSeries[1].Visible = false;
                    }
                }

                switch (ID)
                {
                    case 1: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(255, 255, 0); break;
                    case 2: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(106, 176, 36); break;
                    case 3: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(68, 207, 194); break;
                    case 4: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(38, 100, 209); break;
                    case 5: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(251, 114, 0); break;
                    case 6: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(141, 41, 41); break;
                    case 7: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(255, 243, 0); break;
                }

                PanoramaThree.BeginUpdate();
                _view.PointLineSeries[0].Points = data;
                _view.PointLineSeries[1].Points = dataSave;
                _asyncSemaphore3.Set();
                PanoramaThree.EndUpdate();
            });
        }

        private void DrawGraphsData4(double[] Points, byte ID, double xMin, double xMax)
        {
            DispatchIfNecessary(() =>
            {
                var _view = PanoramaFour.ViewXY;

                int countPoint = Points.Length;
                var data = new SeriesPoint[countPoint];
                var dataSave = new SeriesPoint[countPoint];

                for (int i = 0; i < countPoint; i++)
                {
                    data[i].X = xMin + i * (xMax - xMin) / (countPoint - 1);
                    data[i].Y = Points[i];
                    dataSave[i].X = xMin + i * (xMax - xMin) / (countPoint - 1);

                    if (BSave4.IsChecked == true)
                    {
                        _view.PointLineSeries[1].Visible = true;

                        if (ArrayPoints_4 != null)
                        {
                            if (ArrayPoints_4[i] < data[i].Y)
                            {
                                ArrayPoints_4[i] = data[i].Y;
                                dataSave[i].Y = ArrayPoints_4[i];
                            }
                            else { dataSave[i].Y = ArrayPoints_4[i]; }
                        }
                        else
                        {
                            ArrayPoints_4[i] = Points[i];
                            dataSave[i].Y = ArrayPoints_4[i];
                        }
                        _view.PointLineSeries[1].Visible = true;
                    }
                    else
                    {
                        _view.PointLineSeries[1].Visible = false;
                    }
                }

                switch (ID)
                {
                    case 1: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(255, 255, 0); break;
                    case 2: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(106, 176, 36); break;
                    case 3: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(68, 207, 194); break;
                    case 4: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(38, 100, 209); break;
                    case 5: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(251, 114, 0); break;
                    case 6: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(141, 41, 41); break;
                    case 7: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(255, 243, 0); break;
                }

                PanoramaFour.BeginUpdate();
                _view.PointLineSeries[0].Points = data;
                _view.PointLineSeries[1].Points = dataSave;
                _asyncSemaphore4.Set();
                PanoramaFour.EndUpdate();
            });
        }

        private void DrawGraphsData5(double[] Points, byte ID, double xMin, double xMax)
        {
            DispatchIfNecessary(() =>
            {
                var _view = PanoramaFive.ViewXY;

                int countPoint = Points.Length;
                var data = new SeriesPoint[countPoint];
                var dataSave = new SeriesPoint[countPoint];

                for (int i = 0; i < countPoint; i++)
                {
                    data[i].X = xMin + i * (xMax - xMin) / (countPoint - 1);
                    data[i].Y = Points[i];
                    dataSave[i].X = xMin + i * (xMax - xMin) / (countPoint - 1);

                    if (BSave5.IsChecked == true)
                    {
                        _view.PointLineSeries[1].Visible = true;

                        if (ArrayPoints_5 != null)
                        {
                            if (ArrayPoints_5[i] < data[i].Y)
                            {
                                ArrayPoints_5[i] = data[i].Y;
                                dataSave[i].Y = ArrayPoints_5[i];
                            }
                            else { dataSave[i].Y = ArrayPoints_5[i]; }
                        }
                        else
                        {
                            ArrayPoints_5[i] = Points[i];
                            dataSave[i].Y = ArrayPoints_5[i];
                        }
                        _view.PointLineSeries[1].Visible = true;
                    }
                    else
                    {
                        _view.PointLineSeries[1].Visible = false;
                    }
                }

                switch (ID)
                {
                    case 1: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(255, 255, 0); break;
                    case 2: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(106, 176, 36); break;
                    case 3: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(68, 207, 194); break;
                    case 4: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(38, 100, 209); break;
                    case 5: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(251, 114, 0); break;
                    case 6: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(141, 41, 41); break;
                    case 7: _view.PointLineSeries[0].LineStyle.Color = System.Windows.Media.Color.FromRgb(255, 243, 0); break;
                }

                PanoramaFive.BeginUpdate();
                _view.PointLineSeries[0].Points = data;
                _view.PointLineSeries[1].Points = dataSave;
                _asyncSemaphore5.Set();
                PanoramaFive.EndUpdate();
            });
        }
        #endregion

        #region BSave
        private void BSave_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void BSave_Unchecked(object sender, RoutedEventArgs e)
        {
            if (PanoramaOne.ViewXY.PointLineSeries[1] != null)
            {
                PanoramaOne.ViewXY.PointLineSeries[1].Clear();
                PanoramaOne.ViewXY.PointLineSeries[1].Visible = false;
            }
            for (int i = 0; i < ArrayPoints_1.Length - 1; i++)
            {
                ArrayPoints_1[i] = -120;
            }
        }

        private void BSave2_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void BSave2_Unchecked(object sender, RoutedEventArgs e)
        {
            if (PanoramaTwo.ViewXY.PointLineSeries[1] != null)
            {
                PanoramaTwo.ViewXY.PointLineSeries[1].Clear();
                PanoramaTwo.ViewXY.PointLineSeries[1].Visible = false;
            }
            for (int i = 0; i < ArrayPoints_2.Length - 1; i++)
            {
                ArrayPoints_2[i] = -120;
            }
        }

        private void BSave3_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void BSave3_Unchecked(object sender, RoutedEventArgs e)
        {
            if (PanoramaThree.ViewXY.PointLineSeries[1] != null)
            {
                PanoramaThree.ViewXY.PointLineSeries[1].Clear();
                PanoramaThree.ViewXY.PointLineSeries[1].Visible = false;
            }
            for (int i = 0; i < ArrayPoints_3.Length - 1; i++)
            {
                ArrayPoints_3[i] = -120;
            }
        }

        private void BSave4_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void BSave4_Unchecked(object sender, RoutedEventArgs e)
        {
            if (PanoramaFour.ViewXY.PointLineSeries[1] != null)
            {
                PanoramaFour.ViewXY.PointLineSeries[1].Clear();
                PanoramaFour.ViewXY.PointLineSeries[1].Visible = false;
            }
            for (int i = 0; i < ArrayPoints_4.Length - 1; i++)
            {
                ArrayPoints_4[i] = -120;
            }
        }

        private void BSave5_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void BSave5_Unchecked(object sender, RoutedEventArgs e)
        {
            if (PanoramaFive.ViewXY.PointLineSeries[1] != null)
            {
                PanoramaFive.ViewXY.PointLineSeries[1].Clear();
                PanoramaFive.ViewXY.PointLineSeries[1].Visible = false;
            }
            for (int i = 0; i < ArrayPoints_5.Length - 1; i++)
            {
                ArrayPoints_5[i] = -120;
            }
        }
        #endregion       

        private void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }
    }
}
