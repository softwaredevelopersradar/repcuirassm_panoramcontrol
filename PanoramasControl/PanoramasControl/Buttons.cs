﻿using DAPServerClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace PanoramasControl
{
    public partial class PanoramasGraph : UserControl
    {
        Client client = new Client();

        #region Buttons_Click
/*
        private void ButOne_Click(object sender, RoutedEventArgs e)
        {
            var xAxes = PanoramaOne.ViewXY.XAxes[0];
            double.TryParse(Tbox1.Text, out double Frq);
            double.TryParse(Tbox2.Text, out double DelFrq);
            double xAxesMin = 10;
            double xAxesMax = 6010;
            
            if (Frq < xAxesMin || Frq > xAxesMax)
            {
                MessageBox.Show("Incorrectly frequency");
            }
            else
            {
                if ((Frq - DelFrq / 2.0) < xAxesMin || (Frq + DelFrq / 2.0) > xAxesMax)
                {
                    if ((Frq - DelFrq / 2.0) < xAxesMin && (Frq + DelFrq / 2.0) > xAxesMax)
                    {
                        xAxes.Minimum = xAxesMin;
                        xAxes.Maximum = xAxesMax;
                        Frequency_1 = (xAxesMax - xAxesMin) / 2.0 + xAxesMin;
                        RangeFrq_1 = (xAxesMax - xAxesMin) / 2.0;
                    }
                    else
                    {
                        if ((Frq - DelFrq / 2.0) < xAxesMin)
                        {
                            xAxes.Minimum = xAxesMin;
                            xAxes.Maximum = Frq + DelFrq / 2.0;
                            Frequency_1 = Frq;
                            RangeFrq_1 = Frq - xAxesMin;
                        }
                        if ((Frq + DelFrq / 2.0) > xAxesMax)
                        {
                            xAxes.Maximum = xAxesMax;
                            xAxes.Minimum = Frq - DelFrq / 2.0;
                            Frequency_1 = Frq;
                            RangeFrq_1 = xAxesMax - Frq;
                        }
                    }                    
                }
                else
                {
                    var _panorama = PanoramaOne.ViewXY;
                    PanoramaOne.Dispatcher.Invoke(delegate ()
                    {
                        _panorama.XAxes[0].Maximum = Frq + DelFrq / 2.0;
                        _panorama.XAxes[0].Minimum = Frq - DelFrq / 2.0;
                        Frequency_1 = Frq;
                        RangeFrq_1 = DelFrq;
                    });

                    
                }
            }
        }

        private void ButTwo_Click(object sender, RoutedEventArgs e)
        {
            var xAxes = PanoramaTwo.ViewXY.XAxes[0];
            double.TryParse(Tbox3.Text, out double Frq);
            double.TryParse(Tbox4.Text, out double DelFrq);
            double xAxesMin = 10;
            double xAxesMax = 6010;

            if (Frq < xAxesMin || Frq > xAxesMax)
            {
                MessageBox.Show("Incorrectly frequency");
            }
            else
            {
                if ((Frq - DelFrq / 2.0) < xAxesMin || (Frq + DelFrq / 2.0) > xAxesMax)
                {
                    if ((Frq - DelFrq / 2.0) < xAxesMin && (Frq + DelFrq / 2.0) > xAxesMax)
                    {
                        xAxes.Minimum = xAxesMin;
                        xAxes.Maximum = xAxesMax;
                        Frequency_2 = (xAxesMax - xAxesMin) / 2.0 + xAxesMin;
                        RangeFrq_2 = (xAxesMax - xAxesMin) / 2.0;
                    }
                    else
                    {
                        if ((Frq - DelFrq / 2.0) < xAxesMin)
                        {
                            xAxes.Minimum = xAxesMin;
                            xAxes.Maximum = Frq + DelFrq / 2.0;
                            Frequency_2 = Frq;
                            RangeFrq_2 = Frq - xAxesMin;
                        }
                        if ((Frq + DelFrq / 2.0) > xAxesMax)
                        {
                            xAxes.Maximum = xAxesMax;
                            xAxes.Minimum = Frq - DelFrq / 2.0;
                            Frequency_2 = Frq;
                            RangeFrq_2 = xAxesMax - Frq;
                        }
                    }
                }
                else
                {
                    PanoramaTwo.Dispatcher.Invoke(delegate ()
                    {
                        var _panorama = PanoramaTwo.ViewXY;

                        _panorama.XAxes[0].Maximum = Frq + DelFrq / 2.0;
                        _panorama.XAxes[0].Minimum = Frq - DelFrq / 2.0;
                        Frequency_2 = Frq;
                        RangeFrq_2 = DelFrq;
                    });
                }
            }
        }

        private void ButThree_Click(object sender, RoutedEventArgs e)
        {
            var xAxes = PanoramaThree.ViewXY.XAxes[0];
            double.TryParse(Tbox5.Text, out double Frq);
            double.TryParse(Tbox6.Text, out double DelFrq);
            double xAxesMin = 10;
            double xAxesMax = 6010;

            if (Frq < xAxesMin || Frq > xAxesMax)
            {
                MessageBox.Show("Incorrectly frequency");
            }
            else
            {
                if ((Frq - DelFrq / 2.0) < xAxesMin || (Frq + DelFrq / 2.0) > xAxesMax)
                {
                    if ((Frq - DelFrq / 2.0) < xAxesMin && (Frq + DelFrq / 2.0) > xAxesMax)
                    {
                        xAxes.Minimum = xAxesMin;
                        xAxes.Maximum = xAxesMax;
                        Frequency_3 = (xAxesMax - xAxesMin) / 2.0 + xAxesMin;
                        RangeFrq_3 = (xAxesMax - xAxesMin) / 2.0;
                    }
                    else
                    {
                        if ((Frq - DelFrq / 2.0) < xAxesMin)
                        {
                            xAxes.Minimum = xAxesMin;
                            xAxes.Maximum = Frq + DelFrq / 2.0;
                            Frequency_3 = Frq;
                            RangeFrq_3 = Frq - xAxesMin;
                        }
                        if ((Frq + DelFrq / 2.0) > xAxesMax)
                        {
                            xAxes.Maximum = xAxesMax;
                            xAxes.Minimum = Frq - DelFrq / 2.0;
                            Frequency_3 = Frq;
                            RangeFrq_3 = xAxesMax - Frq;
                        }
                    }
                }
                else
                {
                    PanoramaThree.Dispatcher.Invoke(delegate ()
                    {
                        var _panorama = PanoramaThree.ViewXY;

                        _panorama.XAxes[0].Maximum = Frq + DelFrq / 2.0;
                        _panorama.XAxes[0].Minimum = Frq - DelFrq / 2.0;
                        Frequency_3 = Frq;
                        RangeFrq_3 = DelFrq;
                    });
                }
            }
        }

        private void ButFour_Click(object sender, RoutedEventArgs e)
        {
            var xAxes = PanoramaFour.ViewXY.XAxes[0];
            double.TryParse(Tbox7.Text, out double Frq);
            double.TryParse(Tbox8.Text, out double DelFrq);
            double xAxesMin = 10;
            double xAxesMax = 6010;

            if (Frq < xAxesMin || Frq > xAxesMax)
            {
                MessageBox.Show("Incorrectly frequency");
            }
            else
            {
                if ((Frq - DelFrq / 2.0) < xAxesMin || (Frq + DelFrq / 2.0) > xAxesMax)
                {
                    if ((Frq - DelFrq / 2.0) < xAxesMin && (Frq + DelFrq / 2.0) > xAxesMax)
                    {
                        xAxes.Minimum = xAxesMin;
                        xAxes.Maximum = xAxesMax;
                        Frequency_4 = (xAxesMax - xAxesMin) / 2.0 + xAxesMin;
                        RangeFrq_4 = (xAxesMax - xAxesMin) / 2.0;
                    }
                    else
                    {
                        if ((Frq - DelFrq / 2.0) < xAxesMin)
                        {
                            xAxes.Minimum = xAxesMin;
                            xAxes.Maximum = Frq + DelFrq / 2.0;
                            Frequency_4 = Frq;
                            RangeFrq_4 = Frq - xAxesMin;
                        }
                        if ((Frq + DelFrq / 2.0) > xAxesMax)
                        {
                            xAxes.Maximum = xAxesMax;
                            xAxes.Minimum = Frq - DelFrq / 2.0;
                            Frequency_4 = Frq;
                            RangeFrq_4 = xAxesMax - Frq;
                        }
                    }
                }
                else
                {
                    PanoramaFour.Dispatcher.Invoke(delegate ()
                    {
                        var _panorama = PanoramaFour.ViewXY;

                        _panorama.XAxes[0].Maximum = Frq + DelFrq / 2.0;
                        _panorama.XAxes[0].Minimum = Frq - DelFrq / 2.0;
                        Frequency_4 = Frq;
                        RangeFrq_4 = DelFrq;
                    });
                }
            }
        }

        private void ButFive_Click(object sender, RoutedEventArgs e)
        {
            var xAxes = PanoramaFive.ViewXY.XAxes[0];
            double.TryParse(Tbox9.Text, out double Frq);
            double.TryParse(Tbox10.Text, out double DelFrq);
            double xAxesMin = 10;
            double xAxesMax = 6010;

            if (Frq < xAxesMin || Frq > xAxesMax)
            {
                MessageBox.Show("Incorrectly frequency");
            }
            else
            {
                if ((Frq - DelFrq / 2.0) < xAxesMin || (Frq + DelFrq / 2.0) > xAxesMax)
                {
                    if ((Frq - DelFrq / 2.0) < xAxesMin && (Frq + DelFrq / 2.0) > xAxesMax)
                    {
                        xAxes.Minimum = xAxesMin;
                        xAxes.Maximum = xAxesMax;
                        Frequency_5 = (xAxesMax - xAxesMin) / 2.0 + xAxesMin;
                        RangeFrq_5 = (xAxesMax - xAxesMin) / 2.0;
                    }
                    else
                    {
                        if ((Frq - DelFrq / 2.0) < xAxesMin)
                        {
                            xAxes.Minimum = xAxesMin;
                            xAxes.Maximum = Frq + DelFrq / 2.0;
                            Frequency_5 = Frq;
                            RangeFrq_5 = Frq - xAxesMin;
                        }
                        if ((Frq + DelFrq / 2.0) > xAxesMax)
                        {
                            xAxes.Maximum = xAxesMax;
                            xAxes.Minimum = Frq - DelFrq / 2.0;
                            Frequency_5 = Frq;
                            RangeFrq_5 = xAxesMax - Frq;
                        }
                    }
                }
                else
                {
                    PanoramaFive.Dispatcher.Invoke(delegate ()
                    {
                        var _panorama = PanoramaFive.ViewXY;

                        _panorama.XAxes[0].Maximum = Frq + DelFrq / 2.0;
                        _panorama.XAxes[0].Minimum = Frq - DelFrq / 2.0;
                        Frequency_5 = Frq;
                        RangeFrq_5 = DelFrq;
                    });
                }
            }
        }
        */
        #endregion
    }
}
