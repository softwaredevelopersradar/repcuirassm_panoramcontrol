﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PanoramasControl
{
    public partial class PanoramasGraph
    {
        GridLengthConverter GridLengthConverter;
        private string height = "*";
        private int countPanoramas = 5;
        public int CountPanoramas 
        {
            get => countPanoramas;
            set 
            {
                countPanoramas = value;
                PanoramasVisibility(countPanoramas);
            }
        }

        private void PanoramasVisibility(int count) 
        {
            switch (count) 
            {
                case 0:
                    break;
                case 1:
                    gridMain.RowDefinitions[0].Height = (GridLength)GridLengthConverter.ConvertFromString(height);
                    gridMain.RowDefinitions[1].Height = new GridLength(0);
                    gridMain.RowDefinitions[2].Height = new GridLength(0);
                    gridMain.RowDefinitions[3].Height = new GridLength(0);
                    gridMain.RowDefinitions[4].Height = new GridLength(0);
                    break;
                case 2:
                    gridMain.RowDefinitions[0].Height = (GridLength)GridLengthConverter.ConvertFromString(height);
                    gridMain.RowDefinitions[1].Height = (GridLength)GridLengthConverter.ConvertFromString(height);
                    gridMain.RowDefinitions[2].Height = new GridLength(0);
                    gridMain.RowDefinitions[3].Height = new GridLength(0);
                    gridMain.RowDefinitions[4].Height = new GridLength(0);
                    break;
                case 3:
                    gridMain.RowDefinitions[0].Height = (GridLength)GridLengthConverter.ConvertFromString(height);
                    gridMain.RowDefinitions[1].Height = (GridLength)GridLengthConverter.ConvertFromString(height);
                    gridMain.RowDefinitions[2].Height = (GridLength)GridLengthConverter.ConvertFromString(height);
                    gridMain.RowDefinitions[3].Height = new GridLength(0);
                    gridMain.RowDefinitions[4].Height = new GridLength(0);
                    break;
                case 4:
                    gridMain.RowDefinitions[0].Height = (GridLength)GridLengthConverter.ConvertFromString(height);
                    gridMain.RowDefinitions[1].Height = (GridLength)GridLengthConverter.ConvertFromString(height);
                    gridMain.RowDefinitions[2].Height = (GridLength)GridLengthConverter.ConvertFromString(height);
                    gridMain.RowDefinitions[3].Height = (GridLength)GridLengthConverter.ConvertFromString(height);
                    gridMain.RowDefinitions[4].Height = new GridLength(0);
                    break;
                case 5:
                    gridMain.RowDefinitions[0].Height = (GridLength)GridLengthConverter.ConvertFromString(height);
                    gridMain.RowDefinitions[1].Height = (GridLength)GridLengthConverter.ConvertFromString(height);
                    gridMain.RowDefinitions[2].Height = (GridLength)GridLengthConverter.ConvertFromString(height);
                    gridMain.RowDefinitions[3].Height = (GridLength)GridLengthConverter.ConvertFromString(height);
                    gridMain.RowDefinitions[4].Height = (GridLength)GridLengthConverter.ConvertFromString(height);
                    break;
            }

        }
    }
}
