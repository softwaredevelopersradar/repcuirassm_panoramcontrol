﻿using System.Collections.Generic;
using System.Windows.Controls;

namespace PanoramasControl
{
    /// <summary>
    /// Логика взаимодействия для PanoramasGraph.xaml
    /// </summary>
    public partial class PanoramasGraph : UserControl
    {
        public PanoramasGraph()
        {
            InitializeComponent();
            GridLengthConverter = new System.Windows.GridLengthConverter();
            InitSettings();
            InitArraySaveData();
            InitComboBox();
            InitBands();
        }

        #region Init
        private void InitSettings()
        {
            isAddF = new List<bool>() { true, true, true, true, true };
        }

        public void InitFrequencyRangeGraph1()
        {
            var panorama = PanoramaOne.ViewXY;

            PanoramaOne.BeginUpdate();

            panorama.XAxes[0].Maximum = Freq_1 + Range_1 / 2.0;
            panorama.XAxes[0].Minimum = PanoramaOne.ViewXY.XAxes[0].Maximum - Range_1;
            panorama.YAxes[0].Maximum = 0;
            panorama.YAxes[0].Minimum = -120;

            PanoramaOne.EndUpdate();           
        }

        public void InitFrequencyRangeGraph2()
        {
            var panorama = PanoramaTwo.ViewXY;

            PanoramaTwo.BeginUpdate();

            panorama.XAxes[0].Maximum = Frequency_2 + Range_2 / 2.0;
            panorama.XAxes[0].Minimum = PanoramaTwo.ViewXY.XAxes[0].Maximum - RangeFrq_2;
            panorama.YAxes[0].Maximum = 0;
            panorama.YAxes[0].Minimum = -120;

            PanoramaTwo.EndUpdate();
        }

        public void InitFrequencyRangeGraph3()
        {
            var panorama = PanoramaThree.ViewXY;

            PanoramaThree.BeginUpdate();

            panorama.XAxes[0].Maximum = Freq_3 + Range_3 / 2.0;
            panorama.XAxes[0].Minimum = PanoramaThree.ViewXY.XAxes[0].Maximum - Range_3;
            panorama.YAxes[0].Maximum = 0;
            panorama.YAxes[0].Minimum = -120;

            PanoramaThree.EndUpdate();
        }

        public void InitFrequencyRangeGraph4()
        {
            var panorama = PanoramaFour.ViewXY;

            PanoramaFour.BeginUpdate();

            panorama.XAxes[0].Maximum = Freq_4 + Range_4 / 2.0;
            panorama.XAxes[0].Minimum = PanoramaFour.ViewXY.XAxes[0].Maximum - Range_4;
            panorama.YAxes[0].Maximum = 0;
            panorama.YAxes[0].Minimum = -120;

            PanoramaFour.EndUpdate();
        }

        public void InitFrequencyRangeGraph5()
        {
            var panorama = PanoramaFive.ViewXY;

            PanoramaFive.BeginUpdate();

            panorama.XAxes[0].Maximum = Freq_5 + Range_5 / 2.0;
            panorama.XAxes[0].Minimum = PanoramaFive.ViewXY.XAxes[0].Maximum - Range_5;
            panorama.YAxes[0].Maximum = 0;
            panorama.YAxes[0].Minimum = -120;

            PanoramaFive.EndUpdate();
        }


        #endregion


    }
}
