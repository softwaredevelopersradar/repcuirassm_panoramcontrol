﻿using Arction.Wpf.SemibindableCharting;
using Arction.Wpf.SemibindableCharting.SeriesXY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MainPanoramaControl
{
    /// <summary>
    /// Логика взаимодействия для MainPanoramaControl.xaml
    /// </summary>
    public partial class MainPanoramaControl : UserControl
    {
        public MainPanoramaControl()
        {
            InitializeComponent();
            InitGraghSettengs();
            GenerateRandomData();
        }

        private void InitGraghSettengs()
        {
            var axesRanges = MainGraph.ViewXY;
            axesRanges.YAxes[0].Minimum = 2400;
            axesRanges.YAxes[0].Maximum = 2500;
            axesRanges.YAxes[1].Minimum = 5720;
            axesRanges.YAxes[1].Maximum = 5871;
        }

        private void GenerateRandomData()
        {
            MainGraph.BeginUpdate();
            Random random = new Random();
            int pointsCountY1 = 8000;
            double intervalY1 = 100 / 8000.0;
            double nextY1 = 0;
                                   
            SeriesPoint[] pointsY1 = new SeriesPoint[pointsCountY1];
            for (int pIndex = 0; pIndex < pointsCountY1; pIndex++)
            {
                pointsY1[pIndex].X = 200.0 * (0.2 * (random.NextDouble() - 0.5) + 0.2 * Math.Sin(nextY1));
                pointsY1[pIndex].Y = 2400 + nextY1;
                nextY1 += intervalY1;
            }

            PointLineSeries sourceY1 = new PointLineSeries(MainGraph.ViewXY, MainGraph.ViewXY.XAxes[0], MainGraph.ViewXY.YAxes[0]);
           
            sourceY1.Points = pointsY1;
            MainGraph.ViewXY.PointLineSeries.Add(sourceY1);
            
            //Y2
            int pointsCountY2 = 8000;
            double intervalY2 = (5870.5 - 5720) / pointsCountY2;
            double nextY2 = 0;

            SeriesPoint[] pointsY2 = new SeriesPoint[pointsCountY2];
            for (int pIndex = 0; pIndex < pointsCountY2; pIndex++)
            {
                pointsY2[pIndex].X = 200.0 * (0.2 * (random.NextDouble() - 0.5) + 0.2 * Math.Sin(nextY2));
                pointsY2[pIndex].Y = 5720 + nextY2;
                nextY2 += intervalY2;
            }

            PointLineSeries sourceY2 = new PointLineSeries(MainGraph.ViewXY, MainGraph.ViewXY.XAxes[0], MainGraph.ViewXY.YAxes[1]);
            sourceY2.Points = pointsY2;
            MainGraph.ViewXY.PointLineSeries.Add(sourceY2);
            MainGraph.EndUpdate();
        }

        private void MainGraph_MouseMove(object sender, MouseEventArgs e)
        {
            MainGraph.ViewXY.XAxes[0].ZoomingEnabled = false;
            MainGraph.ViewXY.YAxes[0].ZoomingEnabled = false;
            MainGraph.ViewXY.YAxes[1].ZoomingEnabled = false;
        }
    }
}
