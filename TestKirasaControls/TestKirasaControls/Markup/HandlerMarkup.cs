﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TestKirasaControls
{
    public partial class MainWindow
    {
        WindowMarkup windowMarkup;
        GridLengthConverter GridLengthConverter = new GridLengthConverter();

        private WindowMarkup LoadMarkup() 
        {
            windowMarkup = YamlLoad<WindowMarkup>("MarkupPanoramas.yaml");
            if (windowMarkup == null)
            {
                windowMarkup = new WindowMarkup();
                YamlSave(windowMarkup, "MarkupPanoramas.yaml");
            }
            return windowMarkup;
        }

        private void InitPanoramasMarkup() 
        {
            windowMarkup = LoadMarkup();
        }
    }
}
