﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestKirasaControls
{
    interface IMurkupSettings
    {
        double PrevSize { get; set; }
        double CurrentSize { get; set; }
    }
}
