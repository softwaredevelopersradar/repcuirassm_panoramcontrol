﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestKirasaControls
{
    public class WindowMarkup : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public WindowMarkup() 
        {
            MainPanoramaMarkupSettings = new MainPanoramaMarkupSettings();
            PanoramasMarkupSettings = new PanoramasMarkupSettings();
            CorrFunctionsMarkupSettings = new CorrFunctionsMarkupSettings();
        }

        public MainPanoramaMarkupSettings MainPanoramaMarkupSettings { get; set; }
        public PanoramasMarkupSettings PanoramasMarkupSettings { get; set; }
        public CorrFunctionsMarkupSettings CorrFunctionsMarkupSettings { get; set; }
    }

    public class MainPanoramaMarkupSettings : IMurkupSettings 
    {
        public MainPanoramaMarkupSettings() 
        {
            PrevSize = 200;
            CurrentSize = 200;
        }

        public double PrevSize { get; set; }
        public double CurrentSize { get; set; }
    }

    public class PanoramasMarkupSettings : IMurkupSettings 
    {
        public PanoramasMarkupSettings()
        {
            PrevSize = 400;
            CurrentSize = 400;
        }
        public double PrevSize { get; set; }
        public double CurrentSize { get; set; }
    }

    public class CorrFunctionsMarkupSettings : IMurkupSettings 
    {
        public CorrFunctionsMarkupSettings() 
        {
            PrevSize = 200;
            CurrentSize = 200;
        }

        public double PrevSize { get; set; }
        public double CurrentSize { get; set; }
    }
}
