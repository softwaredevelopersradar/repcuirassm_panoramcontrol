﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TestKirasaControls
{
    public partial class MainWindow
    {
        int countRightPanoramas = 0;
        int CountRightPanoramas 
        {
            get => countRightPanoramas;
            set { countRightPanoramas = value; }
        }

        int countLeftPanoramas = 0;
        int CountLeftPanoramas 
        {
            get => countLeftPanoramas;
            set { countLeftPanoramas = value; }
        }

        CountPanoramas countPanoramas;
        private CountPanoramas LoadComboboxData() 
        {
            countPanoramas = YamlLoad<CountPanoramas>("PanoramaWindowSettings.yaml");
            if (countPanoramas == null)
            {
                countPanoramas = new CountPanoramas();
                YamlSave(countPanoramas, "PanoramaWindowSettings.yaml");
            }
            return countPanoramas;
        }
               
        private void InitComboxSettings() 
        {
            countPanoramas = LoadComboboxData();

            pLibrary.CountPanoramas = countPanoramas.ComboboxSettings.CountPanoramas;
            //pLibrary.CountCorrPanoramas = countPanoramas.ComboboxSettings.CountCorrPanoramas;

            switch (countPanoramas.ComboboxSettings.CountCorrPanoramas)
            {
                case 0:
                    pLibrary.CorrPanoramaIsVisibility(PanoramaLibrary.PLibrary.CorrelationVisibility.False);
                    break;
                case 1:
                    pLibrary.CorrPanoramaIsVisibility(PanoramaLibrary.PLibrary.CorrelationVisibility.True);
                    break;
                default:
                    break;
            }
            
            PanoramasGraphs.IndGraph = countPanoramas.SelectedPanorama.Index;
            PanoramasGraphs.MaxAmplitudeYAxe = countPanoramas.PanoramaSettings.MaxAmplitudeYAxe;

            CountLeftPanoramas = countPanoramas.ComboboxSettings.CountPanoramas;
            CountRightPanoramas = countPanoramas.ComboboxSettings.CountCorrPanoramas;
        }

        private void PLibrary_OnSendCBIndex(int count)
        {
            CountLeftPanoramas = count;

            if (count != 0) 
            {
                PanoramasGraphs.CountPanoramas = count;

                countPanoramas.ComboboxSettings.CountPanoramas = count;
                countPanoramas.SelectedPanorama.Index = PanoramasGraphs.IndGraph;
                

                gridMain.RowDefinitions[2].Height = (GridLength)GridLengthConverter.ConvertFromString("2*");

                if(countRightPanoramas != 0) 
                {
                    gridDown.ColumnDefinitions[0].Width = (GridLength)GridLengthConverter.ConvertFromString("2*");
                    gridDown.ColumnDefinitions[2].Width = (GridLength)GridLengthConverter.ConvertFromString("*");
                }
                else 
                {
                    gridDown.ColumnDefinitions[0].Width = (GridLength)GridLengthConverter.ConvertFromString("*");
                    gridDown.ColumnDefinitions[2].Width = (GridLength)GridLengthConverter.ConvertFromString("0");
                }
            }
            else 
            {
                countPanoramas.ComboboxSettings.CountPanoramas = count;
                countPanoramas.SelectedPanorama.Index = PanoramasGraphs.IndGraph;

                if (gridDown.ColumnDefinitions[2].Width == (GridLength)GridLengthConverter.ConvertFromString("0")) 
                {
                    gridMain.RowDefinitions[2].Height = (GridLength)GridLengthConverter.ConvertFromString("0");
                }
                else 
                {
                    gridDown.ColumnDefinitions[0].Width = (GridLength)GridLengthConverter.ConvertFromString("0");
                    gridDown.ColumnDefinitions[2].Width = (GridLength)GridLengthConverter.ConvertFromString("*");
                }                
            }

            YamlSave(countPanoramas, "PanoramaWindowSettings.yaml");
        }

        private void PLibrary_OnSendCBCorrIndex(int count)
        {
            if(count != 0) 
            {
                CorrFunctionControl.CountPanoramas = count;

                countPanoramas.ComboboxSettings.CountCorrPanoramas = count;
                CountRightPanoramas = count;

                gridMain.RowDefinitions[2].Height = (GridLength)GridLengthConverter.ConvertFromString("2*");

                if(countLeftPanoramas != 0) 
                {
                    gridDown.ColumnDefinitions[0].Width = (GridLength)GridLengthConverter.ConvertFromString("2*");
                    gridDown.ColumnDefinitions[2].Width = (GridLength)GridLengthConverter.ConvertFromString("*");
                }
                else 
                {
                    gridDown.ColumnDefinitions[0].Width = (GridLength)GridLengthConverter.ConvertFromString("0");
                    gridDown.ColumnDefinitions[2].Width = (GridLength)GridLengthConverter.ConvertFromString("*");
                }
            }
            else 
            {
                if(gridDown.ColumnDefinitions[0].Width == (GridLength)GridLengthConverter.ConvertFromString("0")) 
                {
                    gridMain.RowDefinitions[2].Height = (GridLength)GridLengthConverter.ConvertFromString("0");
                }
                else 
                {
                    //gridDown.Height = (GridLength)GridLengthConverter.ConvertFromString("2*");
                    gridDown.ColumnDefinitions[0].Width = (GridLength)GridLengthConverter.ConvertFromString("*");
                    gridDown.ColumnDefinitions[2].Width = (GridLength)GridLengthConverter.ConvertFromString("0");
                }
            }
                        
            YamlSave(countPanoramas, "CountPanoramas.yaml");
        }

        private void PanoramasGraphs_OnCheckGraph(object sender, byte index)
        {
            countPanoramas.SelectedPanorama.Index = PanoramasGraphs.IndGraph;
            countPanoramas.ComboboxSettings.CountPanoramas = PanoramasGraphs.CountPanoramas;

            YamlSave(countPanoramas, "PanoramaWindowSettings.yaml");
        }

        private void PLibrary_OnCorrelationVisibility(PanoramaLibrary.PLibrary.CorrelationVisibility correlationVisibility)
        {
            switch (correlationVisibility)
            {
                case PanoramaLibrary.PLibrary.CorrelationVisibility.True:

                    countPanoramas.ComboboxSettings.CountCorrPanoramas = 1;
                    CountRightPanoramas = 1;
                    gridMain.RowDefinitions[2].Height = (GridLength)GridLengthConverter.ConvertFromString("2*");

                    if (countLeftPanoramas != 0)
                    {
                        gridDown.ColumnDefinitions[0].Width = (GridLength)GridLengthConverter.ConvertFromString("2*");
                        gridDown.ColumnDefinitions[2].Width = (GridLength)GridLengthConverter.ConvertFromString("*");
                    }
                    else
                    {
                        gridDown.ColumnDefinitions[0].Width = (GridLength)GridLengthConverter.ConvertFromString("0");
                        gridDown.ColumnDefinitions[2].Width = (GridLength)GridLengthConverter.ConvertFromString("*");
                    }
                    break;
                case PanoramaLibrary.PLibrary.CorrelationVisibility.False:
                    countPanoramas.ComboboxSettings.CountCorrPanoramas = 0;
                    CountRightPanoramas = 0;
                    if (gridDown.ColumnDefinitions[0].Width == (GridLength)GridLengthConverter.ConvertFromString("0"))
                    {
                        gridMain.RowDefinitions[2].Height = (GridLength)GridLengthConverter.ConvertFromString("0");
                    }
                    else
                    {
                        //gridDown.Height = (GridLength)GridLengthConverter.ConvertFromString("2*");
                        gridDown.ColumnDefinitions[0].Width = (GridLength)GridLengthConverter.ConvertFromString("*");
                        gridDown.ColumnDefinitions[2].Width = (GridLength)GridLengthConverter.ConvertFromString("0");
                    }
                    break;
                default:
                    break;
            }
            YamlSave(countPanoramas, "PanoramaWindowSettings.yaml");
        }
    }
}
