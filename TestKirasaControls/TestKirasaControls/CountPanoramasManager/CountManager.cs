﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestKirasaControls
{
    public class CountPanoramas : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public CountPanoramas()
        {
            ComboboxSettings = new ComboboxSettings();
            SelectedPanorama = new SelectedPanorama();
            PanoramaSettings = new PanoramaSettings();
        }
        public ComboboxSettings ComboboxSettings { get; set; }
        public SelectedPanorama SelectedPanorama { get; set; }
        public PanoramaSettings PanoramaSettings { get; set; }
    }

    public class ComboboxSettings
    {
        public ComboboxSettings()
        {
            CountPanoramas = 5;
            CountCorrPanoramas = 1;
        }

        public int CountPanoramas { get; set; }
        public int CountCorrPanoramas { get; set; }
    }

    public class SelectedPanorama
    {
        public SelectedPanorama()
        {
            Index = 1;
            IndexCorr = 1;
        }

        public byte Index { get; set; }
        public byte IndexCorr { get; set; }
    }

    public class PanoramaSettings
    {
        public PanoramaSettings()
        {
            MaxAmplitudeYAxe = -50;
        }

        public int MaxAmplitudeYAxe { get; set; }
    }
}
