﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestKirasaControls
{
    partial class MainWindow
    {
        void UpdateCorrGraphProperties() 
        {
            CorrFunctionControl.XAxesCorrGraphSetMin = -4000;
            CorrFunctionControl.XAxesCorrGraphSetMax = 4000;
            CorrFunctionControl.CountPointsSet = 2001;
        }

        private void InitDefaultChannels()
        {
            pLibrary.DefaultChannels(true, true, true, true);
        }
    }
}
