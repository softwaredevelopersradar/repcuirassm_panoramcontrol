﻿using DAPprotocols;
using DAPServerClient2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestKirasaControls
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Client client = new Client();
        UserControl CorrFuncControl;
        bool isConnect = false;

        public MainWindow()
        {
            InitializeComponent();
            ServerConnection.ShowDisconnect();

            InitDAPClientEvents();
            InitPLibrary();
            InitDefaultChannels();
            InitPLIbraryEvents();
            InitTextBoxManager();
            InitComboxSettings();
            InitPanoramsGraph();
            UpdateCorrGraphProperties();
            bAdmin.IsChecked = true;
        }
        
        private void InitTextBoxManager() 
        {
            var textBoxSettings = LoadTextBoxData();

            PanoramasGraphs.Frequency_1 = textBoxSettings.textBoxSettings.Frequency_1;
            PanoramasGraphs.RangeFrq_1 = textBoxSettings.textBoxSettings.Range_1;
            PanoramasGraphs.Frequency_2 = textBoxSettings.textBoxSettings.Frequency_2;
            PanoramasGraphs.RangeFrq_2 = textBoxSettings.textBoxSettings.Range_2;
            PanoramasGraphs.Frequency_3 = textBoxSettings.textBoxSettings.Frequency_3;
            PanoramasGraphs.RangeFrq_3 = textBoxSettings.textBoxSettings.Range_3;
            PanoramasGraphs.Frequency_4 = textBoxSettings.textBoxSettings.Frequency_4;
            PanoramasGraphs.RangeFrq_4 = textBoxSettings.textBoxSettings.Range_4;
            PanoramasGraphs.Frequency_5 = textBoxSettings.textBoxSettings.Frequency_5;
            PanoramasGraphs.RangeFrq_5 = textBoxSettings.textBoxSettings.Range_5;
        }

        private void InitPLibrary()
        {
            pLibrary.ViewMode = 1;
            pLibrary.GlobalRangeXmin = 10;
            pLibrary.GlobalBandWidthMHz = 62.5;
            pLibrary.GlobalNumberOfBands = 96;
            pLibrary.GlobalRangeYmin = -120;
            pLibrary.GlobalRangeYmax = 0;
        }

        private void InitPLIbraryEvents()
        {
            pLibrary.NeedSpectrumRequest += PLibrary_NeedSpectrumRequest;
            pLibrary.ThresholdChange += PLibrary_ThresholdChangeAsync;
            pLibrary.NeedExBearingRequest += PLibrary_NeedExBearingRequest;
            pLibrary.ApplyFilterValue += PLibrary_ApplyFilterValue;
            pLibrary.OnSendCBIndex += PLibrary_OnSendCBIndex;
            pLibrary.OnSendCBCorrIndex += PLibrary_OnSendCBCorrIndex;
            pLibrary.OnSetFreqToPanoram += PLibrary_OnSetFreqToPanoram;
            pLibrary.OnCorrelationVisibility += PLibrary_OnCorrelationVisibility;
        }

        private void PLibrary_OnSetFreqToPanoram(object sender, double begin, double end, double frequency)
        {
            double range = Math.Abs(end - begin);
            //var comboboxSettings = LoadComboboxData();
            switch (PanoramasGraphs.IndGraph) 
            {
                case 1:
                    PanoramasGraphs.Frequency_1 = frequency;
                    PanoramasGraphs.RangeFrq_1 = range;
                    break;
                case 2:
                    PanoramasGraphs.Frequency_2 = frequency;
                    PanoramasGraphs.RangeFrq_2 = range;
                    break;
                case 3:
                    PanoramasGraphs.Frequency_3 = frequency;
                    PanoramasGraphs.RangeFrq_3 = range;
                    break;
                case 4:
                    PanoramasGraphs.Frequency_4 = frequency;
                    PanoramasGraphs.RangeFrq_4 = range;
                    break;
                case 5:
                    PanoramasGraphs.Frequency_5 = frequency;
                    PanoramasGraphs.RangeFrq_5 = range;
                    break;
                default:
                    PanoramasGraphs.Frequency_1 = frequency;
                    PanoramasGraphs.RangeFrq_1 = range;
                    break;
            }

            var textBoxSettings = new TextBoxData();
            textBoxSettings.textBoxSettings.Frequency_1 = PanoramasGraphs.Frequency_1;
            textBoxSettings.textBoxSettings.Range_1 = PanoramasGraphs.RangeFrq_1;
            textBoxSettings.textBoxSettings.Frequency_2 = PanoramasGraphs.Frequency_2;
            textBoxSettings.textBoxSettings.Range_2 = PanoramasGraphs.RangeFrq_2;
            textBoxSettings.textBoxSettings.Frequency_3 = PanoramasGraphs.Frequency_3;
            textBoxSettings.textBoxSettings.Range_3 = PanoramasGraphs.RangeFrq_3;
            textBoxSettings.textBoxSettings.Frequency_4 = PanoramasGraphs.Frequency_4;
            textBoxSettings.textBoxSettings.Range_4 = PanoramasGraphs.RangeFrq_4;
            textBoxSettings.textBoxSettings.Frequency_5 = PanoramasGraphs.Frequency_5;
            textBoxSettings.textBoxSettings.Range_5 = PanoramasGraphs.RangeFrq_5;
            YamlSave(textBoxSettings, "TextBoxData.yaml");
        }

        private async void PLibrary_NeedExBearingRequest(object sender, double MinBandX, double MaxBandX, int PhAvCount, int PlAvCount)
        {
            //запрос коор функции
            Console.WriteLine($"{MinBandX} - {MaxBandX}");
            var answer = await client.GetCorrFunc(0, MinBandX, MaxBandX);
           //var answer1 = await client.GetCorrFunc(1, MinBandX, MaxBandX);
        }

        private async void PLibrary_ApplyFilterValue(object sender, bool value)
        {
            var answer = await client.ApplyExFilter(value);
        }

        private void InitPanoramsGraph()
        {
            PanoramasGraphs.NeedSpectrumRequest += PanoramasGraphs_NeedSpectrumRequest;
            PanoramasGraphs.NeedSpectrumRequest2 += PanoramasGraphs_NeedSpectrumRequest2;
            PanoramasGraphs.NeedSpectrumRequest3 += PanoramasGraphs_NeedSpectrumRequest3;
            PanoramasGraphs.NeedSpectrumRequest4 += PanoramasGraphs_NeedSpectrumRequest4;
            PanoramasGraphs.NeedSpectrumRequest5 += PanoramasGraphs_NeedSpectrumRequest5;
            PanoramasGraphs.OnCheckGraph += PanoramasGraphs_OnCheckGraph;
        }

        private async void PanoramasGraphs_NeedSpectrumRequest5(object sender, byte ID, double MinVisibleX, double MaxVisibleX, int PointCount)
        {
            var answer = new GetSpectrumResponse();

            switch (ID)
            {
                case 1: answer = await client.GetSpectrum(5, ChannelPicker.Channel1, MinVisibleX, MaxVisibleX, PointCount); break;
                case 2: answer = await client.GetSpectrum(5, ChannelPicker.Channel2, MinVisibleX, MaxVisibleX, PointCount); break;
                case 3: answer = await client.GetSpectrum(5, ChannelPicker.Channel3, MinVisibleX, MaxVisibleX, PointCount); break;
                case 4: answer = await client.GetSpectrum(5, ChannelPicker.Channel4, MinVisibleX, MaxVisibleX, PointCount); break;
                case 5: answer = await client.GetSpectrum(5, ChannelPicker.ChannelMax, MinVisibleX, MaxVisibleX, PointCount); break;
                case 6: answer = await client.GetSpectrum(5, ChannelPicker.ChannelMedian, MinVisibleX, MaxVisibleX, PointCount); break;
                case 7: answer = await client.GetSpectrum(5, ChannelPicker.ChannelSum, MinVisibleX, MaxVisibleX, PointCount); break;
            }

            AnswerGetSpectrum(answer);
        }

        private async void PanoramasGraphs_NeedSpectrumRequest4(object sender, byte ID, double MinVisibleX, double MaxVisibleX, int PointCount)
        {
            var answer = new GetSpectrumResponse();

            switch (ID)
            {
                case 1: answer = await client.GetSpectrum(4, ChannelPicker.Channel1, MinVisibleX, MaxVisibleX, PointCount); break;
                case 2: answer = await client.GetSpectrum(4, ChannelPicker.Channel2, MinVisibleX, MaxVisibleX, PointCount); break;
                case 3: answer = await client.GetSpectrum(4, ChannelPicker.Channel3, MinVisibleX, MaxVisibleX, PointCount); break;
                case 4: answer = await client.GetSpectrum(4, ChannelPicker.Channel4, MinVisibleX, MaxVisibleX, PointCount); break;
                case 5: answer = await client.GetSpectrum(4, ChannelPicker.ChannelMax, MinVisibleX, MaxVisibleX, PointCount); break;
                case 6: answer = await client.GetSpectrum(4, ChannelPicker.ChannelMedian, MinVisibleX, MaxVisibleX, PointCount); break;
                case 7: answer = await client.GetSpectrum(4, ChannelPicker.ChannelSum, MinVisibleX, MaxVisibleX, PointCount); break;
            }

            AnswerGetSpectrum(answer);
        }

        private async void PanoramasGraphs_NeedSpectrumRequest3(object sender, byte ID, double MinVisibleX, double MaxVisibleX, int PointCount)
        {
            var answer = new GetSpectrumResponse();

            switch (ID)
            {
                case 1: answer = await client.GetSpectrum(3, ChannelPicker.Channel1, MinVisibleX, MaxVisibleX, PointCount); break;
                case 2: answer = await client.GetSpectrum(3, ChannelPicker.Channel2, MinVisibleX, MaxVisibleX, PointCount); break;
                case 3: answer = await client.GetSpectrum(3, ChannelPicker.Channel3, MinVisibleX, MaxVisibleX, PointCount); break;
                case 4: answer = await client.GetSpectrum(3, ChannelPicker.Channel4, MinVisibleX, MaxVisibleX, PointCount); break;
                case 5: answer = await client.GetSpectrum(3, ChannelPicker.ChannelMax, MinVisibleX, MaxVisibleX, PointCount); break;
                case 6: answer = await client.GetSpectrum(3, ChannelPicker.ChannelMedian, MinVisibleX, MaxVisibleX, PointCount); break;
                case 7: answer = await client.GetSpectrum(3, ChannelPicker.ChannelSum, MinVisibleX, MaxVisibleX, PointCount); break;
            }

            AnswerGetSpectrum(answer);
        }

        private async void PanoramasGraphs_NeedSpectrumRequest2(object sender, byte ID, double MinVisibleX, double MaxVisibleX, int PointCount)
        {
            var answer = new GetSpectrumResponse();

            switch (ID)
            {
                case 1: answer = await client.GetSpectrum(2, ChannelPicker.Channel1, MinVisibleX, MaxVisibleX, PointCount); break;
                case 2: answer = await client.GetSpectrum(2, ChannelPicker.Channel2, MinVisibleX, MaxVisibleX, PointCount); break;
                case 3: answer = await client.GetSpectrum(2, ChannelPicker.Channel3, MinVisibleX, MaxVisibleX, PointCount); break;
                case 4: answer = await client.GetSpectrum(2, ChannelPicker.Channel4, MinVisibleX, MaxVisibleX, PointCount); break;
                case 5: answer = await client.GetSpectrum(2, ChannelPicker.ChannelMax, MinVisibleX, MaxVisibleX, PointCount); break;
                case 6: answer = await client.GetSpectrum(2, ChannelPicker.ChannelMedian, MinVisibleX, MaxVisibleX, PointCount); break;
                case 7: answer = await client.GetSpectrum(2, ChannelPicker.ChannelSum, MinVisibleX, MaxVisibleX, PointCount); break;
            }

            AnswerGetSpectrum(answer);
        }

        private async void PanoramasGraphs_NeedSpectrumRequest(object sender, byte ID, double MinVisibleX, double MaxVisibleX, int PointCount)
        {
            var answer = new GetSpectrumResponse();

            switch (ID)
            {
                case 1: answer = await client.GetSpectrum(1, ChannelPicker.Channel1, MinVisibleX, MaxVisibleX, PointCount); break;
                case 2: answer = await client.GetSpectrum(1, ChannelPicker.Channel2, MinVisibleX, MaxVisibleX, PointCount); break;
                case 3: answer = await client.GetSpectrum(1, ChannelPicker.Channel3, MinVisibleX, MaxVisibleX, PointCount); break;
                case 4: answer = await client.GetSpectrum(1, ChannelPicker.Channel4, MinVisibleX, MaxVisibleX, PointCount); break;
                case 5: answer = await client.GetSpectrum(1, ChannelPicker.ChannelMax, MinVisibleX, MaxVisibleX, PointCount); break;
                case 6: answer = await client.GetSpectrum(1, ChannelPicker.ChannelMedian, MinVisibleX, MaxVisibleX, PointCount); break;
                case 7: answer = await client.GetSpectrum(1, ChannelPicker.ChannelSum, MinVisibleX, MaxVisibleX, PointCount); break;
            }

            AnswerGetSpectrum(answer);
        }

        private void InitDAPClientEvents()
        {
            client.IsConnected += Client_IsConnected;
            client.IsRead += Client_ClientIsRead;
            client.IsWrite += Client_ClientIsWrite;
            
            client.GetExtraordinaryModeMessageUpdate += Client_AnswerSetMode;

            client.GetCorrFuncUpdate += Client_AnswerGetCorrFunc;
        }

        private async void ServerConnection_ButServerClick(object sender, RoutedEventArgs e)
        {
            DispatchIfNecessary(async () => 
            {
                if (!isConnect)
                {
                    CancellationTokenSource cts = new CancellationTokenSource();
                    CancellationToken token = cts.Token;
                    int timeout = 3000;
                    var task = client.ConnectToServer2("127.0.0.1", token);
                    var task2 = W3s(timeout);

                    if (await Task.WhenAny(task, task2) == task)
                    {
                        Console.WriteLine("task completed within timeout");
                        ServerConnection.ShowConnect();
                        ServerConnection.ColorRead = Brushes.Green;
                        ServerConnection.ColorWrite = Brushes.Green;
                        ServerConnection.ShowRead();
                        ServerConnection.ShowWrite();
                    }
                    else
                    {
                        cts.Cancel();
                        ServerConnection.ShowDisconnect();
                        ServerConnection.ColorRead = Brushes.Red;
                        ServerConnection.ColorWrite = Brushes.Red;
                        ServerConnection.ShowRead();
                        ServerConnection.ShowWrite();
                        Console.WriteLine("timeout logic");
                        MessageBox.Show("Invalid IP address or Server not found", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }

                    Console.WriteLine("continue work");

                    async Task W3s(int t)
                    {
                        await Task.Delay(t);
                    }
                }
                else
                {
                    client.DisconnectFromServer();
                    pLibrary.Mode = 0;
                    ServerConnection.ShowDisconnect();
                    ServerConnection.ColorRead = Brushes.Red;
                    ServerConnection.ColorWrite = Brushes.Red;
                    ServerConnection.ShowRead();
                    ServerConnection.ShowWrite();
                }
            });

            
            
        }

        private async void BModeStop_Click(object sender, RoutedEventArgs e)
        {
            //отправка режима работы 0 на сервер
            var answer = await client.SetMode(DapServerMode.Stop);
            pLibrary.Mode = 0;
            PanoramasGraphs.UpdateMode(0);
        }

        private async void BModeRI_Click(object sender, RoutedEventArgs e)
        {
            //отправка режима работы 1 на сервер
            var answer = await client.SetMode(DapServerMode.RadioIntelligence);
            pLibrary.Mode = 1;
            PanoramasGraphs.UpdateMode(1);
        }

        private async void PLibrary_NeedSpectrumRequest(object sender, double MinVisibleX, double MaxVisibleX, int PointCount, byte id)
        {
            //запрос спектра от сервера
            var answer = new GetSpectrumResponse();
            switch (id)
            {
                case 1: answer = await client.GetSpectrum(0, ChannelPicker.Channel1, MinVisibleX, MaxVisibleX, PointCount); break;
                case 2: answer = await client.GetSpectrum(0, ChannelPicker.Channel2, MinVisibleX, MaxVisibleX, PointCount); break;
                case 3: answer = await client.GetSpectrum(0, ChannelPicker.Channel3, MinVisibleX, MaxVisibleX, PointCount); break;
                case 4: answer = await client.GetSpectrum(0, ChannelPicker.Channel4, MinVisibleX, MaxVisibleX, PointCount); break;
                case 5: answer = await client.GetSpectrum(0, ChannelPicker.ChannelMax, MinVisibleX, MaxVisibleX, PointCount); break;
                case 6: answer = await client.GetSpectrum(0, ChannelPicker.ChannelMedian, MinVisibleX, MaxVisibleX, PointCount); break;
                case 7: answer = await client.GetSpectrum(0, ChannelPicker.ChannelSum, MinVisibleX, MaxVisibleX, PointCount); break;
            }

            AnswerGetSpectrum(answer);
        }        

        private async void PLibrary_ThresholdChangeAsync(object sender, int value)
        {
            //отправить на сервер
            //var answer = await client.SetFilters((short)value);
            //или передать куда ещё, а потом на сервер
        }

        private async void GetCorrFunnc_Click(object sender, RoutedEventArgs e)
        {
            var answer = await client.GetCorrFunc(0, 20, 50);
        }

        private void CorrFunctionControl_Loaded(object sender, RoutedEventArgs e)
        {
            CorrFuncControl = new UserControl();
            CorrFunctionControl.InitializeComponent();            
        }

        private void cbLang_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (cbLang.SelectedIndex) 
            {
                case 0: PanoramasGraphs.Language("Ru"); break;
                case 1: PanoramasGraphs.Language("En"); break;
            }
        }

        private void bAdmin_Click(object sender, RoutedEventArgs e)
        {

        }

        private void bAdmin_Checked(object sender, RoutedEventArgs e)
        {
            CorrFunctionControl.DebugVisible = 1;
            bAdmin.Content = "Admin";
        }

        private void bAdmin_Unchecked(object sender, RoutedEventArgs e)
        {
            CorrFunctionControl.DebugVisible = 0;
            bAdmin.Content = "User";
        }
    }
}
