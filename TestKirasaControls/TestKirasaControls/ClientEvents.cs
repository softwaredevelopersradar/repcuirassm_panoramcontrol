﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using DAPServerClient2;

namespace TestKirasaControls
{
    public partial class MainWindow : Window
    {
        private void Client_IsConnected(bool isConnected)
        {
            DispatchIfNecessary(() => 
            {
                isConnect = isConnected;
                isConnect = isConnected;
                if (isConnected) ServerConnection.ShowConnect();
                else ServerConnection.ShowDisconnect();
            });            
        }

        private void Client_ClientIsRead(bool isRead)
        {
            //ServerConnection.ShowRead();
        }

        private void Client_ClientIsWrite(bool isWrite)
        {
            //ServerConnection.ShowWrite();
        }

        private void Client_AnswerSetMode(DAPprotocols.ModeMessage modeMessage)
        {
            if (modeMessage?.Header.ErrorCode == 0)
            {
                if(modeMessage.Mode == DAPprotocols.DapServerMode.Stop)
                {
                    pLibrary.Mode = 0;
                    PanoramasGraphs.UpdateMode(0);
                }
                if (modeMessage.Mode == DAPprotocols.DapServerMode.RadioIntelligence)
                {
                    pLibrary.Mode = 1;
                    PanoramasGraphs.UpdateMode(1);
                }
            }
        }

        private void Client_AnswerSetFrequency(DAPprotocols.DefaultMessage defaultMessage)
        {
        }

        private void AnswerGetSpectrum(DAPprotocols.GetSpectrumResponse spectrumResponse)
        {
            if (spectrumResponse?.Header.ErrorCode == 0)
            {
                switch (spectrumResponse.PostNumber)
                {
                    case 0:
                        if (spectrumResponse.Spectrum != null)
                        {
                            //Draw Spectrum Main
                            switch (spectrumResponse.Picker) 
                            {
                                case DAPprotocols.ChannelPicker.Channel1: pLibrary.IQSpectrumPainted(spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz, spectrumResponse.Spectrum, 1); break;
                                case DAPprotocols.ChannelPicker.Channel2: pLibrary.IQSpectrumPainted(spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz, spectrumResponse.Spectrum, 2); break;
                                case DAPprotocols.ChannelPicker.Channel3: pLibrary.IQSpectrumPainted(spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz, spectrumResponse.Spectrum, 3); break;
                                case DAPprotocols.ChannelPicker.Channel4: pLibrary.IQSpectrumPainted(spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz, spectrumResponse.Spectrum, 4); break;
                                case DAPprotocols.ChannelPicker.ChannelMax: pLibrary.IQSpectrumPainted(spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz, spectrumResponse.Spectrum, 5); break;
                                case DAPprotocols.ChannelPicker.ChannelMedian: pLibrary.IQSpectrumPainted(spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz, spectrumResponse.Spectrum, 6); break;
                            }                           
                        }
                        break;
                    case 1:
                        if (spectrumResponse.Spectrum != null)
                        {
                            //Draw Spectrum First 
                            switch (spectrumResponse.Picker)
                            {
                                case DAPprotocols.ChannelPicker.Channel1: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 1, 1, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                case DAPprotocols.ChannelPicker.Channel2: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 1, 2, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                case DAPprotocols.ChannelPicker.Channel3: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 1, 3, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                case DAPprotocols.ChannelPicker.Channel4: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 1, 4, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                case DAPprotocols.ChannelPicker.ChannelMax: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 1, 5, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                case DAPprotocols.ChannelPicker.ChannelMedian: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 1, 6, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                            }
                        }
                        break;
                    case 2:
                        if (spectrumResponse.Spectrum != null)
                        {
                            //Draw Spectrum Second 
                            switch (spectrumResponse.Picker)
                            {
                                case DAPprotocols.ChannelPicker.Channel1: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 2, 1, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                case DAPprotocols.ChannelPicker.Channel2: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 2, 2, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                case DAPprotocols.ChannelPicker.Channel3: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 2, 3, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                case DAPprotocols.ChannelPicker.Channel4: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 2, 4, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                case DAPprotocols.ChannelPicker.ChannelMax: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 2, 5, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                case DAPprotocols.ChannelPicker.ChannelMedian: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 2, 6, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                            }
                        }
                        break;
                    case 3:
                        if (spectrumResponse.Spectrum != null)
                        {
                            //Draw Spectrum Third
                            switch (spectrumResponse.Picker)
                            {
                                case DAPprotocols.ChannelPicker.Channel1: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 3, 1, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                case DAPprotocols.ChannelPicker.Channel2: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 3, 2, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                case DAPprotocols.ChannelPicker.Channel3: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 3, 3, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                case DAPprotocols.ChannelPicker.Channel4: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 3, 4, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                case DAPprotocols.ChannelPicker.ChannelMax: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 3, 5, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                case DAPprotocols.ChannelPicker.ChannelMedian: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 3, 6, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                            }
                        }
                        break;
                    case 4:
                        if (spectrumResponse.Spectrum != null)
                        {
                            //Draw Spectrum Four 
                            switch (spectrumResponse.Picker)
                            {
                                case DAPprotocols.ChannelPicker.Channel1: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 4, 1, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                case DAPprotocols.ChannelPicker.Channel2: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 4, 2, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                case DAPprotocols.ChannelPicker.Channel3: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 4, 3, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                case DAPprotocols.ChannelPicker.Channel4: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 4, 4, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                case DAPprotocols.ChannelPicker.ChannelMax: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 4, 5, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                case DAPprotocols.ChannelPicker.ChannelMedian: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 4, 6, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                            }
                        }
                        break;
                    case 5:
                        if (spectrumResponse.Spectrum != null)
                        {
                            //Five
                            switch (spectrumResponse.Picker)
                            {
                                case DAPprotocols.ChannelPicker.Channel1: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 5, 1, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                case DAPprotocols.ChannelPicker.Channel2: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 5, 2, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                case DAPprotocols.ChannelPicker.Channel3: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 5, 3, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                case DAPprotocols.ChannelPicker.Channel4: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 5, 4, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                case DAPprotocols.ChannelPicker.ChannelMax: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 5, 5, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                case DAPprotocols.ChannelPicker.ChannelMedian: PanoramasGraphs.Converter(spectrumResponse.Spectrum, 5, 6, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                            }
                        }
                        break;                    
                }                
            }
        }

        private void Client_AnswerGetCorrFunc(DAPprotocols.GetCorrFuncResponse corrFuncResponse)
        {
            if (corrFuncResponse?.Header.ErrorCode == 0)
            {
                if (corrFuncResponse.CorrFunc0 != null || corrFuncResponse.CorrFunc1 != null || corrFuncResponse.CorrFunc2 != null)
                {                   
                    CorrFunctionControl.Converter(corrFuncResponse.CorrFunc0, 1, corrFuncResponse.FuncNumber, corrFuncResponse.FrequencykHz, corrFuncResponse.BandwidthkHz);
                    CorrFunctionControl.Converter(corrFuncResponse.CorrFunc1, 2, corrFuncResponse.FuncNumber, corrFuncResponse.FrequencykHz, corrFuncResponse.BandwidthkHz);
                    CorrFunctionControl.Converter(corrFuncResponse.CorrFunc2, 3, corrFuncResponse.FuncNumber, corrFuncResponse.FrequencykHz, corrFuncResponse.BandwidthkHz);
                    CorrFunctionControl.Converter(corrFuncResponse.CorrFunc3, 0, corrFuncResponse.FuncNumber, corrFuncResponse.FrequencykHz, corrFuncResponse.BandwidthkHz);
                }
            }
        }

        public void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }
    }
}
